<?php require_once("init.inc.php") ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no">
        <title>ESHOP</title>
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,500,500i,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link href="../css/style.css" rel="stylesheet" type="text/css">
        <link href="../css/style_page_catalogue.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <?php require "../pages/header.html"; ?>
            <!--MAIN-->            
            <main class="container">
                <div class="row">
                    <!--FIL ARIANE-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../index.php">Accueil</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Page catalogue extérieur</li>
                        </ol>
                    </nav>
                </div>
                <!--NOS PRODUITS-->
                <section class="row ligne_produits">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 en-tete"><h3>EXTERIEUR</h3>
                                <span class="desc-catalogue">
                                    Un carré potager, une terrasse pour les soirées d’été avec salon et barbecue, une piscine, un terrain de jeu pour les enfants équipé d’un portique, … Le jardin est un espace de liberté à inventer, aménager, cultiver. Leroy Merlin vous donne les clefs pour créer le jardin qui vous ressemble depuis les aménagements (abri, terrasse, mobilier, clôture) jusqu’à l’entretien ..
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <aside class="col-md-3 filtre">
                                <h4 class="filtre-aside">Filtrer par</h4>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapseRetrait" aria-expanded="false">
                                    Options de retrait</a>
                                </span>
                                <ul class="list-options collapse" id="collapseRetrait">
                                    <li>
                                        <input type="checkbox" id="magasin" name="magasin" />
                                        <label class="checkbox-label" for="magasin">Retrait en magasin</label></li>
                                    <li>
                                        <input type="checkbox" id="domicile" name="domicile" />
                                        <label class="checkbox-label" for="domicile">Livraison à domicile</label>
                                    </li>
                                </ul>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapseProduit" aria-expanded="false">
                                    Type de produit</a>
                                </span>
                                <ul class="list-options collapse" id="collapseProduit">
                                    <li>
                                        <input type="checkbox" id="piscines" name="piscines" />
                                        <label class="checkbox-label" for="piscines">Piscines et spa</label>
									</li>
                                    <li>
                                        <input type="checkbox" id="abris" name="abris" />
                                        <label class="checkbox-label" for="abris">Abris de jardin</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="salon" name="salon" />
                                        <label class="checkbox-label" for="salon">Salon de jardin</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="cuisine-ext" name="cuisine-ext" />
                                        <label class="checkbox-label" for="cuisine-ext">Cuisine d'extérieur</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="jeux" name="jeux" />
                                        <label class="checkbox-label" for="jeux">Jeux d'extérieur</label>
                                    </li>
                                </ul>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapseMatiere" aria-expanded="false">
                                    Matière</a>
                                </span>
                                <ul class="list-options collapse" id="collapseMatiere">
                                    <li>
                                        <input type="checkbox" id="bois" name="bois" />
                                        <label class="checkbox-label" for="bois">Bois</label>
									</li>
                                    <li>
                                        <input type="checkbox" id="teck" name="teck" />
                                        <label class="checkbox-label" for="teck">Teck</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="metal" name="metal" />
                                        <label class="checkbox-label" for="metal">Métal</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="plastique" name="plastique" />
                                        <label class="checkbox-label" for="plastique">Plastique</label>
                                    </li>
                                </ul>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapsePrix" aria-expanded="false">
                                    Prix</a>
                                </span>
                                <ul class="list-options collapse" id="collapsePrix">
                                    <li>
                                        <input type="checkbox" id="20" name="20" />
                                        <label class="checkbox-label" for="20">20,00€ - 59,99€</label></li>
                                    <li>
                                        <input type="checkbox" id="60" name="60" />
                                        <label class="checkbox-label" for="60">60,00€ - 99,99€</label></li>
                                    <li>
                                    <li>
                                        <input type="checkbox" id="100" name="100" />
                                        <label class="checkbox-label" for="100">100,00€ - 999,99€</label></li>
                                    <li>
                                    <li>
                                        <input type="checkbox" id="1000" name="1000" />
                                        <label class="checkbox-label" for="1000">1000,00€ - 10000,00€</label></li>
                                    <li>
                                </ul>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapseEval" aria-expanded="false">
                                    Evaluation</a>
                                </span>
                                <ul class="list-options collapse" id="collapseEval">
                                    <li>
                                        <input type="checkbox" id="5" name="5" />
                                        <label class="checkbox-label" for="5">5 <span class="fa fa-star checked"></span>
                                        </label></li>
                                    <li>
                                        <input type="checkbox" id="4" name="4" />
                                        <label class="checkbox-label" for="4">4 <span class="fa fa-star checked"></span> ou plus
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="3" name="3" />
                                        <label class="checkbox-label" for="3">3 <span class="fa fa-star checked"></span> ou plus
                                        </label>
                                    </li>
                                </ul>

                            </aside>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_exterieur.php" title ="Piscine extérieure">
                                            <img src='<?= URL ?>img/catalogue_exterieur/img1.jpg' alt ="Parquet chêne">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Piscine extérieure hors sol</p>
                                                <p class='prix'>4999 € TTC</p>
                                            </div>
                                        </div>
				
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_exterieur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_exterieur.php" title ="Cheminée mexicaine">
                                            <img src='<?= URL ?>img/catalogue_exterieur/img2.jpg'  alt ="Cheminée mexicaine">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Cheminée mexicaine</p>
                                                <p class='prix'>159 € TTC</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_exterieur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_exterieur.php" title ="Salon de jardin St tropez taupe">
                                            <img src='<?= URL ?>img/catalogue_exterieur/img3.jpg' alt ="Salon de jardin St tropez taupe">
                                        </a>
                                        <p><span>livraison offerte</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Salon de jardin St tropez taupe</p>
                                                <p class='prix'>716 € TTC l'ensemble</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_exterieur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre offset-md-2 side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_exterieur.php" title ="Aire de jeux bois 2 swing extra TRIGANO">
                                            <img src='<?= URL ?>img/catalogue_exterieur/img4.jpg' alt ="Aire de jeux bois 2 swing extra TRIGANO">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Aire de jeux bois 2 swing extra TRIGANO</p>
                                                <p class='prix'>879 € TTC / unité</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_exterieur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_exterieur.php" title ="Fontaine robinet en zinc">
                                            <img src='<?= URL ?>img/catalogue_exterieur/img5.jpg' alt ="Fontaine robinet en zinc">
                                        </a>
                                        <p><span>livraison offerte</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Fontaine robinet en zinc</p>
                                                <p class='prix'>1499.99 € TTC / unité</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_exterieur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_exterieur.php" title ="Abri de jardin Kerno axess">
                                            <img src='<?= URL ?>img/catalogue_exterieur/img6.jpg' alt ="Abri de jardin Kerno axess">
                                        </a>
                                        <p><span>290 € d'économie</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Abri de jardin Kerno axess</p>
                                                <p class='prix'>989 € / unité</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_exterieur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                </div>
                                <nav aria-label="Page navigation example" class="nav-pagination">
                                    <ul class="pagination">
                                        <li class="page-item">
                                            <a class="page-link" href="#" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                        </li>
                                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item">
                                            <a class="page-link" href="#" aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </section>

            </main>
        </div>
        <?php require "../pages/footer.html"; ?>
        <!-- FIXED HEADER -->
        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function(){
                var offset = $(".menu").offset().top;
                $(document).scroll(function(){
                    var scrollTop = $(document).scrollTop();
                    if(scrollTop > offset){
                        $(".menu").addClass('fixed-top');
                    }
                    else {
                        $(".menu").removeClass('fixed-top');
                    }
                });
            });
        </script>
        <!-- FILTER -->

    </body>
</html>