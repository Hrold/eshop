<?php require_once("init.inc.php") ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no">
        <title>ESHOP</title>
        <link rel="icon" type="image/png" sizes="32x32" href="<?= URL ?>img/favicon-32x32.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,500,500i,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link href="<?= URL ?>css/style.css" rel="stylesheet" type="text/css">
        <link href="<?= URL ?>css/style_contact.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <?php require "header.html"; ?>
            <!--MAIN-->            
            <main class="container">
                <div class="row">
                    <!--FIL ARIANE-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a title="homepage eshop" href="<?= URL ?>index.php">Accueil</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Pose</li>
                        </ol>
                    </nav>
                </div>

                <!--NEW CODE-->
                <div class="row">
                    <div class="col-md-8 mx-auto">
                        <h3 class="mb-2">Contactez-nous pour toute demande d'installation</h3>
                        <span class="before-faq mb-4"><b>Vous nous faites confiance...</b><br>
                            <span>Et vous souhaitez faire appel à notre expertise pour votre installation  à domicile<br></span></span>
                        <form class="needs-validation" novalidate>
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label for="raison">Votre souhaitez faire poser</label>
                                    <select class="custom-select d-block w-100" id="raison" required="">
                                        <option value="1" data-value="1" >Une motorisation de porte de garage</option>
                                        <option value="2" data-value="2" >Un climatiseur fixe</option>
                                        <option value="3" data-value="3" >Un volet roulant sur fenêtre deux vantaux</option>
                                        <option value="4" data-value="4" >Une porte de garage</option>
                                        <option value="5" data-value="5" >Une serrure 3/5 points</option>
                                        <option value="6" data-value="6" >Un plan de travail stratifié</option>
                                        <option value="7" data-value="7" >Du carrelage pour le sol</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-2">
                                    <span class="civilite-title">Civilité</span>
                                    <div class="custom-control custom-radio">
                                        <input id="credit" name="civilite" type="radio" class="custom-control-input" required="">
                                        <label class="custom-control-label" for="credit">Madame</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input id="monsieur" name="civilite" type="radio" class="custom-control-input" required="">
                                        <label class="custom-control-label" for="monsieur">Monsieur</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="prenom">Prénom</label>
                                    <input type="text" class="form-control" id="prenom" placeholder="" value="" required="">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="nom">Nom</label>
                                    <input type="text" class="form-control" id="nom" placeholder="" value="" required="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="email">Adresse e-mail</label>
                                    <input type="email" class="form-control" id="email" placeholder="">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="telephone">N° de téléphone</label>
                                    <input type="telephone" class="form-control" id="telephone" placeholder="">
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label for="fidelite">N° de carte fidélité</label>
                                    <input type="text" class="form-control" id="fidelite" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">                                    <label for="message">Décrivez-nous votre projet</label>
                                <textarea class="form-control" id="message" rows="3"></textarea>
                            </div>

                            <button class="mb-3 btn btn-primary btn-lg btn-block" type="submit">Valider</button>
                        </form>
                    </div>
                </div>



            </main>
        </div>
        <?php require "../pages/footer.html"; ?>

        <!-- FIXED HEADER -->

        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function(){
                var offset = $(".menu").offset().top;
                $(document).scroll(function(){
                    var scrollTop = $(document).scrollTop();
                    if(scrollTop > offset){
                        $(".menu").addClass('fixed-top');
                    }
                    else {
                        $(".menu").removeClass('fixed-top');
                    }
                });
            });
        </script>
    </body>
</html>