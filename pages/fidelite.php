<?php require_once("init.inc.php") ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no">
        <title>ESHOP</title>
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,500,500i,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link href="../css/style.css" rel="stylesheet" type="text/css">
        <link href="../css/style_informations.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <?php require "../pages/header.html"; ?>
            <!--MAIN-->            
            <main class="container">
                <div class="row">
                    <!--FIL ARIANE-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../index.php">Accueil</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Points de fidélité</li>
                        </ol>
                    </nav>
                </div>

                <div class="row">
                    <?php require "../pages/side-nav.html"; ?>
                    <div class="col-lg-9 col-md-12">
                        <h3 class="title-section-produits">Points de fidélité</h3>
                        <hr>
                        <div class="row">
                            <div class='col-md-6 col-sm-12'>
                                <h4>Quels sont les avantages ?</h4>
                                <p class="espace">Vous bénéficiez d’offres exclusives, vous recevez nos actualités en avant-première et même une surprise pour votre anniversaire. Dès votre 2ème achat*, vous cumulez des points valables 24 mois à échanger contre des remises.</p>
                                <p class="espace">* réalisé dans les 12 mois après votre premier achat</p>
                            </div>
                            <div class='col-md-6 col-sm-12'>
                                <h4>Comment obtenir des points ?</h4>
                                <p class="espace"><b>1 €</b> d'achat = 1 point (valable 24 mois)</p>
                                <p class="espace"><b>1</b> commande = 5 points (en boutique ou en ligne)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <?php require "../pages/footer.html"; ?>
        <!-- FIXED HEADER -->
        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function(){
                var offset = $(".menu").offset().top;
                $(document).scroll(function(){
                    var scrollTop = $(document).scrollTop();
                    if(scrollTop > offset){
                        $(".menu").addClass('fixed-top');
                    }
                    else {
                        $(".menu").removeClass('fixed-top');
                    }
                });
            });
        </script>
        <!-- FILTER -->

    </body>
</html>