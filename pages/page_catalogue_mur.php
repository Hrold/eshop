<?php require_once("init.inc.php") ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no">
        <title>ESHOP</title>
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,500,500i,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link href="../css/style.css" rel="stylesheet" type="text/css">
        <link href="../css/style_page_catalogue.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <?php require "../pages/header.html"; ?>
            <!--MAIN-->            
            <main class="container">
                <div class="row">
                    <!--FIL ARIANE-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../index.php">Accueil</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Page catalogue mur</li>
                        </ol>
                    </nav>
                </div>
                <!--NOS PRODUITS-->
                <section class="row ligne_produits">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 en-tete"><h3>MUR</h3>
                                <span class="desc-catalogue">
                                    Vous cherchez des briques, du carrelage mural ou en manque d'inspiration pour vos murs? Avec ESHOP, vous trouverez tout ce qu'il vous faut pour trouver tout ce qu'il vous faut pour faire des murs qui vous ressemblent.
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <aside class="col-md-3 filtre">
                                <h4 class="filtre-aside">Filtrer par</h4>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapseRetrait" aria-expanded="false">
                                    Options de retrait</a>
                                </span>
                                <ul class="list-options collapse" id="collapseRetrait">
                                    <li>
                                        <input type="checkbox" id="magasin" name="magasin" />
                                        <label class="checkbox-label" for="magasin">Retrait en magasin</label></li>
                                    <li>
                                        <input type="checkbox" id="domicile" name="domicile" />
                                        <label class="checkbox-label" for="domicile">Livraison à domicile</label>
                                    </li>
                                </ul>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapseProduit" aria-expanded="false">
                                    Type de produit</a>
                                </span>
                                <ul class="list-options collapse" id="collapseProduit">
                                    <li>
                                        <input type="checkbox" id="laine-verre" name="laine-verre" />
                                        <label class="checkbox-label" for="laine-verre">Laine de verre</label>
									</li>
                                    <li>
                                        <input type="checkbox" id="isolation" name="isolation" />
                                        <label class="checkbox-label" for="isolation">Isolation murale</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="lambris" name="lambris" />
                                        <label class="checkbox-label" for="lambris">Lambris</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="brique" name="brique" />
                                        <label class="checkbox-label" for="brique">Brique</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="carrelage-mur" name="carrelage-mur" />
                                        <label class="checkbox-label" for="carrelage-mur">Carrelage mural</label>
                                    </li>
                                </ul>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapseCouleur" aria-expanded="false">
                                    Couleur</a>
                                </span>
                                <ul class="list-options collapse" id="collapseCouleur">
                                    <li>
                                        <input type="checkbox" id="noir" name="noir" />
                                        <label class="checkbox-label" for="noir">Noir</label>
									</li>
                                    <li>
                                        <input type="checkbox" id="blanc" name="blanc" />
                                        <label class="checkbox-label" for="blanc">Blanc</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="rouge" name="rouge" />
                                        <label class="checkbox-label" for="rouge">Rouge</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="gris" name="gris" />
                                        <label class="checkbox-label" for="gris">Gris</label>
                                    </li>
                                </ul>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapsePrix" aria-expanded="false">
                                    Prix</a>
                                </span>
                                <ul class="list-options collapse" id="collapsePrix">
                                    <li>
                                        <input type="checkbox" id="1" name="1" />
                                        <label class="checkbox-label" for="1">1,00€ - 9,99€</label></li>
                                    <li>
                                        <input type="checkbox" id="10" name="10" />
                                        <label class="checkbox-label" for="10">10,00€ - 29,99€</label></li>
                                    <li>
                                    <li>
                                        <input type="checkbox" id="30" name="30" />
                                        <label class="checkbox-label" for="30">30,00€ - 49,99€</label></li>
                                    <li>
                                    <li>
                                        <input type="checkbox" id="50" name="50" />
                                        <label class="checkbox-label" for="50">50,00€ - 99.99€</label></li>
                                    <li>
									<li>
                                        <input type="checkbox" id="100" name="100" />
                                        <label class="checkbox-label" for="100">100,00€ - 500€</label></li>
                                    <li>
                                </ul>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapseEval" aria-expanded="false">
                                    Evaluation</a>
                                </span>
                                <ul class="list-options collapse" id="collapseEval">
                                    <li>
                                        <input type="checkbox" id="5" name="5" />
                                        <label class="checkbox-label" for="5">5 <span class="fa fa-star checked"></span>
                                        </label></li>
                                    <li>
                                        <input type="checkbox" id="4" name="4" />
                                        <label class="checkbox-label" for="4">4 <span class="fa fa-star checked"></span> ou plus
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="3" name="3" />
                                        <label class="checkbox-label" for="3">3 <span class="fa fa-star checked"></span> ou plus
                                        </label>
                                    </li>
                                </ul>

                            </aside>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_mur.php" title ="Panneau roulé en laine de verre">
                                            <img src='<?= URL ?>img/catalogue_mur/img1.jpg' alt ="Panneau roulé en laine de verre">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Panneau roulé en laine de verre</p>
                                                <p class='prix'>28,60 € TTC</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_mur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_mur.php" title="Plaque de plâtre archi MO H1">
                                            <img src='<?= URL ?>img/catalogue_mur/img2.jpg'  alt ="Plaque de plâtre archi MO H1">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Plaque de plâtre archi MO H1</p>
                                                <p class='prix'>19,95 € TTC</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_mur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_mur.php" title ="Lambris PVC Décor mural bois naturel ">
                                            <img src='<?= URL ?>img/catalogue_mur/img3.jpg' alt ="Lambris PVC Décor mural bois naturel ">
                                        </a>
                                       
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Lambris PVC Décor mural bois naturel </p>
                                                <p class='prix'>79,90 € TTC</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_mur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre offset-md-2 side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_mur.php" title ="Carrelage mur gris, Vestige">
                                            <img src='<?= URL ?>img/catalogue_mur/img4.jpg' alt ="Carrelage mur gris, Vestige">
                                        </a>
										<p><span>- 26 %</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Carrelage mur gris, Vestige</p>
                                                <p class='prix'>19,90 € TTC / m²</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_mur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_mur.php" title ="Brique creuse">
                                            <img src='<?= URL ?>img/catalogue_mur/img5.jpg' alt ="Brique creuse">
                                        </a>

                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Brique creuse</p>
                                                <p class='prix'>2,05 € TTC / unité</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_mur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_mur.php" title ="Brique de verre">
                                            <img src='<?= URL ?>img/catalogue_mur/img6.jpg' alt ="Brique de verre">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Brique de verre</p>
                                                <p class='prix'>1,69 € / unité</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_mur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                </div>
                                <nav aria-label="Page navigation example" class="nav-pagination">
                                    <ul class="pagination">
                                        <li class="page-item">
                                            <a class="page-link" href="#" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                        </li>
                                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item">
                                            <a class="page-link" href="#" aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </section>

            </main>
        </div>
        <?php require "../pages/footer.html"; ?>
        <!-- FIXED HEADER -->
        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function(){
                var offset = $(".menu").offset().top;
                $(document).scroll(function(){
                    var scrollTop = $(document).scrollTop();
                    if(scrollTop > offset){
                        $(".menu").addClass('fixed-top');
                    }
                    else {
                        $(".menu").removeClass('fixed-top');
                    }
                });
            });
        </script>
        <!-- FILTER -->

    </body>
</html>