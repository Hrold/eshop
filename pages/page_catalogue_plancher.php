<?php require_once("init.inc.php") ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no">
        <title>ESHOP</title>
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,500,500i,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link href="../css/style.css" rel="stylesheet" type="text/css">
        <link href="../css/style_page_catalogue.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <?php require "../pages/header.html"; ?>
            <!--MAIN-->            
            <main class="container">
                <div class="row">
                    <!--FIL ARIANE-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../index.php">Accueil</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Page catalogue parquet</li>
                        </ol>
                    </nav>
                </div>
                <!--NOS PRODUITS-->
                <section class="row ligne_produits">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 en-tete"><h3>PARQUET</h3>
                                <span class="desc-catalogue">
                                    Considérés par beaucoup de personnes comme les revêtements de sol les plus fastueux, les parquets et planchers donnent une grande élégance aux pièces d’une maison ou d'un appartement. L’offre de ESHOP comprend des sols stratifiés, ainsi que des parquets contrecollés et massifs dans de magnifiques essences de bois.
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <aside class="col-md-3 filtre">
                                <h4 class="filtre-aside">Filtrer par</h4>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapseRetrait" aria-expanded="false">
                                    Options de retrait</a>
                                </span>
                                <ul class="list-options collapse" id="collapseRetrait">
                                    <li>
                                        <input type="checkbox" id="magasin" name="magasin" />
                                        <label class="checkbox-label" for="magasin">Retrait en magasin</label></li>
                                    <li>
                                        <input type="checkbox" id="domicile" name="domicile" />
                                        <label class="checkbox-label" for="domicile">Livraison à domicile</label>
                                    </li>
                                </ul>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapseProduit" aria-expanded="false">
                                    Type de produit</a>
                                </span>
                                <ul class="list-options collapse" id="collapseProduit">
                                    <li>
                                        <input type="checkbox" id="parquet" name="parquet" />
                                        <label class="checkbox-label" for="parquet">Parquet</label>
                                    </li>
                                </ul>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapseMatiere" aria-expanded="false">
                                    Matière</a>
                                </span>
                                <ul class="list-options collapse" id="collapseMatiere">
                                    <li>
                                        <input type="checkbox" id="chene" name="chene" />
                                        <label class="checkbox-label" for="chene">Chêne</label></li>
                                    <li>
                                        <input type="checkbox" id="massif" name="massif" />
                                        <label class="checkbox-label" for="massif">Massif</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="contrecolle" name="contrecolle" />
                                        <label class="checkbox-label" for="contrecolle">Contre collé</label>
                                    </li>
                                </ul>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapseCouleur" aria-expanded="false">
                                    Couleur</a>
                                </span>
                                <ul class="list-options collapse" id="collapseCouleur">
                                    <li>
                                        <input type="checkbox" id="brun" name="brun" />
                                        <label class="checkbox-label" for="brun">Brun</label></li>
                                    <li>
                                        <input type="checkbox" id="gris" name="gris" />
                                        <label class="checkbox-label" for="gris">Gris</label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="blanc" name="blanc" />
                                        <label class="checkbox-label" for="blanc">Blanc</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="noir" name="noir" />
                                        <label class="checkbox-label" for="noir">Noir</label>
                                    </li>
                                </ul>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapsePrix" aria-expanded="false">
                                    Prix</a>
                                </span>
                                <ul class="list-options collapse" id="collapsePrix">
                                    <li>
                                        <input type="checkbox" id="20" name="20" />
                                        <label class="checkbox-label" for="20">20,00€ - 30,00€</label></li>
                                    <li>
                                        <input type="checkbox" id="30" name="30" />
                                        <label class="checkbox-label" for="30">30,00€ - 40,00€</label></li>
                                    <li>
                                    <li>
                                        <input type="checkbox" id="40" name="40" />
                                        <label class="checkbox-label" for="40">40,00€ - 50,00€</label></li>
                                    <li>
                                    <li>
                                        <input type="checkbox" id="50" name="50" />
                                        <label class="checkbox-label" for="50">50,00€ - 60,00€</label></li>
                                    <li>
                                </ul>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapseEval" aria-expanded="false">
                                    Evaluation</a>
                                </span>
                                <ul class="list-options collapse" id="collapseEval">
                                    <li>
                                        <input type="checkbox" id="5" name="5" />
                                        <label class="checkbox-label" for="5">5 <span class="fa fa-star checked"></span>
                                        </label></li>
                                    <li>
                                        <input type="checkbox" id="4" name="4" />
                                        <label class="checkbox-label" for="4">4 <span class="fa fa-star checked"></span> ou plus
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="3" name="3" />
                                        <label class="checkbox-label" for="3">3 <span class="fa fa-star checked"></span> ou plus
                                        </label>
                                    </li>
                                </ul>

                            </aside>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_plancher.php" title ="Parquet chêne">
                                            <img src='<?= URL ?>img/catalogue_plancher/img1.jpg' alt ="Parquet chêne">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Parquet chêne</p>
                                                <p class='prix'>39.99 € TTC / m²</p>
                                            </div>
                                        </div>
										<p><span>promo - 20%</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_plancher.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_plancher.php" title ="Parquet chêne blanchi">
                                            <img src='<?= URL ?>img/catalogue_plancher/img2.jpg'  alt ="Parquet chêne blanchi">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Parquet chêne blanchi</p>
                                                <p class='prix'>49.99 € TTC / m²</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_plancher.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_plancher.php" title ="Contrecollé chêne, vitrifié">
                                            <img src='<?= URL ?>img/catalogue_plancher/img3.jpg' alt ="Contrecollé chêne, vitrifié">
                                        </a>
                                        <p><span>livraison offerte</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Contrecollé chêne, vitrifié</p>
                                                <p class='prix'>19.99 € TTC / m²</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_plancher.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre offset-md-2 side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_plancher.php" title ="Stratifié Lazio">
                                            <img src='<?= URL ?>img/catalogue_plancher/img4.jpg' alt ="Stratifié Lazio">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Stratifié Lazio</p>
                                                <p class='prix'>19.99 € TTC / m²</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_plancher.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_plancher.php" title ="Plancher brut en pin">
                                            <img src='<?= URL ?>img/catalogue_plancher/img5.jpg' alt ="Plancher brut en pin">
                                        </a>
                                        <p><span>livraison offerte</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Plancher brut en Pin</p>
                                                <p class='prix'>59.99 € TTC / m²</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_plancher.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_plancher.php" title ="plancher massif sapin">
                                            <img src='<?= URL ?>img/catalogue_plancher/img6.jpg' alt ="plancher massif sapin">
                                        </a>
                                        <p><span>livraison offerte</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Plancher massif sapin</p>
                                                <p class='prix'>19.99 € TTC / m²</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_plancher.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                </div>
                                <nav aria-label="Page navigation example" class="nav-pagination">
                                    <ul class="pagination">
                                        <li class="page-item">
                                            <a class="page-link" href="#" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                        </li>
                                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item">
                                            <a class="page-link" href="#" aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </section>

            </main>
        </div>
        <?php require "../pages/footer.html"; ?>
        <!-- FIXED HEADER -->
        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function(){
                var offset = $(".menu").offset().top;
                $(document).scroll(function(){
                    var scrollTop = $(document).scrollTop();
                    if(scrollTop > offset){
                        $(".menu").addClass('fixed-top');
                    }
                    else {
                        $(".menu").removeClass('fixed-top');
                    }
                });
            });
        </script>
        <!-- FILTER -->

    </body>
</html>