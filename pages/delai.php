<?php require_once("init.inc.php") ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no">
        <title>ESHOP</title>
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,500,500i,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link href="../css/style.css" rel="stylesheet" type="text/css">
        <link href="../css/style_informations.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <?php require "../pages/header.html"; ?>
            <!--MAIN-->            
            <main class="container">
                <div class="row">
                    <!--FIL ARIANE-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../index.php">Accueil</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Délais et frais de livraison</li>
                        </ol>
                    </nav>
                </div>
                <div class="row">
                    <?php require "../pages/side-nav.html"; ?>
                    <div class="col-lg-9 col-md-12">
                        <h3 class="title-section-produits">délais et frais de livraison</h3>
                        <hr>
                        <div class="row">
                            <div class="text col-12 text-center">
                                <h4>Bénéficiez de la solution de transport la plus adaptée au contenu de votre commande.</h4>
                                <img src="<?= URL ?>img/informations/livraison.jpg" class="img-fluid" alt="Responsive image">
                            </div>
                            <div class="text col_md-6 col-sm-12">
                                <h4>Colis petits et légers</h4>
                                <p class="espace">
                                    Le numéro de suivi de votre commande vous sera transmis dès l'expédition de votre commande.
                                </p>
                                <p class="espace">
                                    En cas d'absence, un avis de passage sera laissé dans votre boîte aux lettres, vous indiquant où récupérer le colis dans un délai de 14 jours.
                                </p>
                                <p class="espace">
                                    Passé ce délai, votre commande nous sera réexpédiée.
                                    Ces colis peuvent être livrés en France Métropolitaine et en Corse. 
                                </p>
                            </div>
                            <div class="text col-md-6 col-sm-12">
                                <h4>Colis lourds et encombrants </h4>
                                <p class="espace">
                                    Lors de l'expédition de votre commande, le transporteur en charge de votre livraison vous invitera à choisir une date de livraison. 
                                </p>
                                <p class="espace">
                                    La livraison intervient entre 8 et 18h, du lundi au vendredi. Le livreur dépose votre commande directement devant chez vous, au pied de votre immeuble ou à l’entrée de votre résidence. 
                                </p>
                                <p class="espace">
                                    Aucune opération de manutention (livraison à l’étage ou dans une pièce particulière) ni de pose ne sera effectuée par le transporteur. 
                                </p>
                                <p class="espace">
                                    Ces commandes sont livrées en France Métropolitaine.
                                </p>
                            </div>
                            <div class="text col-md-6 col-sm-12">
                                <h4>Transport avec engin embarqué</h4>
                                <p class="espace">
                                    Certains articles très volumineux nécessitent un transport particulier avec engin embarqué. Si tel est le cas pour votre achat, cela vous sera indiqué lors de votre commande. 
                                </p>
                                <p class="espace">
                                    Afin de garantir une livraison dans les meilleures conditions, et une fois que le transporteur sera en possession de votre commande, il vous sera demandé de fournir des précisions sur l'accessibilité de votre propriété. 
                                </p>
                                <p class="espace">
                                    La livraison aura lieu dans votre jardin ou les limites de votre résidence, du lundi au vendredi entre 8h et 18h.
                                </p>
                                <p class="espace">
                                    Ces commandes sont livrées en France Métropolitaine.
                                </p>
                            </div>
                            <div class="text col-md-6 col-sm-12">
                                <h4>Délais de livraison</h4>
                                <p class="espace">
                                    Pour les colis petits et légers : 2 à 4 jours ouvrés.
                                </p>
                                <p class="espace">
                                    Pour les colis lourds et encombrants : 3 à 10 jours ouvrés, selon la date de livraison que vous aurez sélectionnée avec votre transporteur. 
                                </p>
                                <p class="espace">
                                    Veuillez noter que si vos articles ne sont pas en stock lors de votre commande, ces délais s'ajoutent aux délais d'approvisionnement. 
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <?php require "../pages/footer.html"; ?>
        <!-- FIXED HEADER -->
        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function(){
                var offset = $(".menu").offset().top;
                $(document).scroll(function(){
                    var scrollTop = $(document).scrollTop();
                    if(scrollTop > offset){
                        $(".menu").addClass('fixed-top');
                    }
                    else {
                        $(".menu").removeClass('fixed-top');
                    }
                });
            });
        </script>
        <!-- FILTER -->

    </body>
</html>