<?php require_once("init.inc.php") ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no">
        <title>ESHOP</title>
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,500,500i,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link href="../css/style.css" rel="stylesheet" type="text/css">
        <link href="../css/style_contact.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <?php require "../pages/header.html"; ?>
            <!--MAIN-->            
            <main class="container">
                <div class="row">
                    <!--FIL ARIANE-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a title="homepage eshop" href="../index.php">Accueil</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Formulaire de contact</li>
                        </ol>
                    </nav>
                </div>

                <!--NEW CODE-->
                <div class="row">
                    <div class="col-md-8 mx-auto">
                        <h3 class="mb-2">Contactez-nous</h3>
                        <span class="before-faq mb-4"><b>Avant de nous contacter...</b><br>
                            <span>Avez-vous pensé à consulter notre liste de questions fréquentes ?<br>La réponse se trouve certainement <a href="#" title="faq">ici...</a></span></span>
                        <form class="needs-validation" novalidate>
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label for="raison">Votre question concerne</label>
                                    <select class="custom-select d-block w-100" id="raison" required="">
                                        <option value="0">Choisissez un type de question</option>
                                        <option value="1" data-value="1" >Une information sur un produit ou un service</option>
                                        <option value="2" data-value="2" >Une information sur une commande passée en magasin</option>
                                        <option value="1" data-value="3" >Une information sur votre compte internaute</option>
                                        <option value="2" data-value="4" >Une commande en retrait 2h</option>
                                        <option value="3" data-value="5" >Une commande passée sur notre site et livrée chez "vous" ou en point relais</option>
                                        <option value="3" data-value="6" >Un retour d'une commande passée sur notre site</option>
                                        <option value="1" data-value="7" >Un conseil de mise en œuvre</option>
                                        <option value="4" data-value="8" >Une réclamation, une remarque, une suggestion</option>
                                    </select>
                                </div>
                                <div class="col-md-12 mb-5">
                                    <label for="num_commande">N° de commande</label>
                                    <input type="text" class="form-control" id="num_commande" placeholder="" value="" required="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-2">
                                    <span class="civilite-title">Civilité</span>
                                    <div class="custom-control custom-radio">
                                        <input id="credit" name="civilite" type="radio" class="custom-control-input" required="">
                                        <label class="custom-control-label" for="credit">Madame</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input id="monsieur" name="civilite" type="radio" class="custom-control-input" required="">
                                        <label class="custom-control-label" for="monsieur">Monsieur</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="prenom">Prénom</label>
                                    <input type="text" class="form-control" id="prenom" placeholder="" value="" required="">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="nom">Nom</label>
                                    <input type="text" class="form-control" id="nom" placeholder="" value="" required="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="email">Adresse e-mail</label>
                                    <input type="email" class="form-control" id="email" placeholder="">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="telephone">N° de téléphone</label>
                                    <input type="telephone" class="form-control" id="telephone" placeholder="">
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label for="fidelite">N° de carte fidélité</label>
                                    <input type="text" class="form-control" id="fidelite" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">                                    <label for="message">Votre message</label>
                                <textarea class="form-control" id="message" rows="3"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="input-file">Votre pièce jointe</label>
                                <input type="file" class="form-control-file" id="input-file">
                            </div>

                            <button class="mb-3 btn btn-primary btn-lg btn-block" type="submit">Valider</button>
                            <div class="row">
                                <div class="col-md-3 mb-3 coord-content">
                                    <span class="fa-stack fa-contact fa-2x">
                                        <i class="fa fa-circle-thin fa-stack-2x"></i>
                                        <i class="fa fa-map-marker fa-stack-1x"></i>
                                    </span>
                                    <p class="coord-title">L'adresse du siège</p><p>30 impasse de l'eshop<br>30470 Vezin le Lévezou</p>
                                </div>
                                <div class="col-md-3 mb-3 coord-content mx-auto">
                                    <span class="fa-stack fa-contact fa-2x">
                                        <i class="fa fa-circle-thin fa-stack-2x"></i>
                                        <i class="fa fa-phone fa-stack-1x"></i>
                                    </span>
                                    <p class="coord-title">Notre assistance téléphonique</p>
                                    <p>+ 33 (0)5 36 65 65 65</p>
                                </div>
                                <div class="col-md-3 mb-3 coord-content">
                                    <span class="fa-stack fa-contact fa-2x">
                                        <i class="fa fa-circle-thin fa-stack-2x"></i>
                                        <i class="fa fa-envelope fa-stack-1x"></i>
                                    </span>
                                    <p class="coord-title">Notre service internet</p>
                                    <p>contact@eshop.fr</p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>



            </main>
        </div>
        <?php require "../pages/footer.html"; ?>

        <!-- FIXED HEADER -->

        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function(){
                var offset = $(".menu").offset().top;
                $(document).scroll(function(){
                    var scrollTop = $(document).scrollTop();
                    if(scrollTop > offset){
                        $(".menu").addClass('fixed-top');
                    }
                    else {
                        $(".menu").removeClass('fixed-top');
                    }
                });
            });
        </script>
    </body>
</html>