<?php require_once("init.inc.php") ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no">
        <title>ESHOP</title>
        <link rel="icon" type="image/png" sizes="32x32" href="<?= URL ?>img/favicon-32x32.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,500,500i,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link href="../css/style.css" rel="stylesheet" type="text/css">
        <link href="../css/style_page_produit.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <?php require "../pages/header.html"; ?>
            <!--MAIN-->            
            <main class="container">
                <div class="row">
                    <!--FIL ARIANE-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= URL ?>index.php">Accueil</a></li>
							<li class="breadcrumb-item"><a href="<?= URL ?>pages/page_catalogue_accessoire.php">Page catalogue accessoire</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Page produit accessoire</li>
                        </ol>
                    </nav>
                </div>
                <div class="row product">
                    <div class="col-lg-4 col-md-6 left">
                        <div class="main_img zoom">
                            <img src="<?= URL ?>img/produits_accessoire/page_produit_1.jpg" id="principale">
                        </div>

                        <div class="side_img">
                            <div class="miniatures">
                                <a href="<?= URL ?>img/produits_accessoire/page_produit_1.jpg" title="Mon image 1" class="thumbnail"><img src="<?= URL ?>img/produits_accessoire/page_produit_1.jpg" alt="un alt très important"></a>
                            </div>

                            <div class="miniatures">
                                <a href="<?= URL ?>img/produits_accessoire/page_produit_2.jpg" title="Mon image 2" class="thumbnail"><img src="<?= URL ?>img/produits_accessoire/page_produit_2.jpg" alt="un alt très important"></a>
                            </div>

                            <div class="miniatures">
                                <a href="<?= URL ?>img/produits_accessoire/page_produit_3.jpg" title="Mon image 3" class="thumbnail"><img src="<?= URL ?>img/produits_accessoire/page_produit_3.jpg" alt="un alt très important"></a>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-8 col-md-6 height detail_product">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="ref">Réf :
                                    5248428776943</span>
                                <h3>Peinture blanc satin RIPOLIN</h3>
                                <span class="price">37.90 € <span class="ttc">TTC<span class="m-2"><br>soit 15,16 € le litre</span></span></span>
                                <div class="clear"></div>
                                <div class="rating">
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="ecrire_avis"><a href="#" title="ecrire_avis">écrire un avis</a></span>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input id="credit" name="shippingMethod" type="radio" class="custom-control-input" checked required>
                                    <label class="custom-control-label" for="credit">Livraison à domicile <br><span class="shipping_libelle"> Livré chez vous le 20/00/2018</span></label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input id="debit" name="shippingMethod" type="radio" class="custom-control-input" required>
                                    <label class="custom-control-label" for="debit">Retrait en magasin <br><span class="shipping_libelle"> Retrait 2h en magasin - <span><a class="choix_mag" href="#">Choisir un magasin</a></span></span></label>
                                </div>
                                <div class="devis"><a href="#" title="devis">
                                    Demandez un devis personnalisé</a>
                                </div>
                                <div class="ajouter-panier">
                                    <label for="quantite">Qté :</label>
                                    <input id="quantite" min="0" title="Quantité produit" max="999" pattern="[0-9]*" value="1" type="number"><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier"><button type="Ajouter au panier" class="btn btn-primary panier">Ajouter au panier</button></a>
                                </div>
                                <div class="wish-retrait">
                                    <button type="Ajouter au panier" class="btn btn-primary wishlist">Ajouter à ma liste d'envies</button>
                                    <button type="Ajouter au panier" class="btn btn-primary dispo">Voir la disponibilité en magasin</button>
                                </div>
                            </div>
                            <!--REASSURANCE-->
                            <div class="col-md-12 reassurance">
                                <div class="row">
                                    <div class="col-lg-3 col-md-6">
                                        <img src="<?= URL ?>img/homepage/icon_paiement.jpg">
                                        <span class="rea_texte">paiement sécurisé</span>
                                    </div>
                                    <div class="col-lg-3 col-md-6 offset-lg-1 offset-md-0">
                                        <img src="<?= URL ?>img/homepage/icon_satisfait.jpg">
                                        <span class="rea_texte">satisfait ou remboursé</span>
                                    </div>
                                    <div class="col-lg-3 col-md-6 offset-lg-1 offset-md-0">
                                        <img src="<?= URL ?>img/homepage/icon_echange.jpg">
                                        <span class="rea_texte">échange et retour facile</span>
                                    </div>
                                </div>
                            </div>
                        </div>                         
                    </div>
                </div>
                <!--MULTI-ONGLETS-->
                <div class="row doc-tech">
                    <div class="col-md-12">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item-tab">
                                <a class="nav-link active" id="infos-tab" data-toggle="tab" href="#infos" role="tab" aria-controls="infos" aria-selected="true">Informations sur le produit</a>
                            </li>
                            <li class="nav-item-tab">
                                <a class="nav-link" id="comm-tab" data-toggle="tab" href="#comm" role="tab" aria-controls="comm" aria-selected="false">Commentaires</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="infos" role="tabpanel" aria-labelledby="infos-tab">
                                <h5>Détail produit :</h5>
                                <span>Blanc. Aspect satin. Pour toutes pièces. Lessivable. Classe COV A+. Rendement 10 m²/l<br></span>
                                <h5>Caractéristiques</h5>
                                <table class="table table-striped table-sm">
                                    <tbody>
                                        <tr>
                                            <th scope="row">Aspect</th>
                                            <td>Satin</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Destination</th>
                                            <td>Toutes pièces</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Contenance (en litres)</th>
                                            <td>Laine de verre</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Nom de la teinte</th>
                                            <td>Blanc</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Echelle de pouvoir couvrant</th>
                                            <td>6</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Emission de COV après 28 jours (en µg/m³)</th>
                                            <td>1000</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Entretien</th>
                                            <td>Lessivable</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--RATING SECTION-->
                            <div class="tab-pane fade" id="comm" role="tabpanel" aria-labelledby="comm-tab">

                                <div class="col-lg-4 col-md-6 pull-left first-line">
                                    <h4 class="title-not">Notation du produit</h4>
                                    <div class="pull-left">
                                        <div class="pull-left notation-star">
                                            <div>5 <span class="fa fa-star checked"></span></div>
                                        </div>
                                        <div class="pull-left notation-bar">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-100" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pull-right notation-avis-number">1</div>
                                    </div>
                                    <div class="pull-left">
                                        <div class="pull-left notation-star">
                                            <div>4 <span class="fa fa-star checked"></span></div>
                                        </div>
                                        <div class="pull-left notation-bar">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-80" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pull-right notation-avis-number">0</div>
                                    </div>
                                    <div class="pull-left">
                                        <div class="pull-left notation-star">
                                            <div>3 <span class="fa fa-star checked"></span></div>
                                        </div>
                                        <div class="pull-left notation-bar">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-60" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="5">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pull-right notation-avis-number">1</div>
                                    </div>
                                    <div class="pull-left">
                                        <div class="pull-left notation-star">
                                            <div>2 <span class="fa fa-star checked"></span></div>
                                        </div>
                                        <div class="pull-left notation-bar">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-40" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="5">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pull-right notation-avis-number">0</div>
                                    </div>
                                    <div class="pull-left">
                                        <div class="pull-left notation-star">
                                            <div>1 <span class="fa fa-star checked"></span></div>
                                        </div>
                                        <div class="pull-left notation-bar">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-20" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="5">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pull-right notation-avis-number">0</div>
                                    </div>
                                </div>

                                <div class="col-md-12 pull-left second-line">
                                    <div class="review-block">
                                        <div class="row">
                                            <div class="col-lg-2 col-sm-3">
                                                <img src="<?= URL ?>img/produits_parquets/user.svg" class="img-user">
                                                <div class="review-block-name"><a href="#">MomoDu93</a></div>
                                                <div class="review-block-date">05 Septembre 2016<br/>Il y a 1 an</div>
                                            </div>
                                            <div class="col-lg-10 col-sm-9 review">
                                                <div class="review-block-rate">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                </div>
                                                <div class="review-block-title">Très bon pouvoir couvrant</div>
                                                <div class="review-block-description">
                                                   Très bonne peinture . Ne coule pas ,agréable à étaler.Peinture posée sur placo enduit (murs neufs).
                                                </div>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="row">
                                            <div class="col-lg-2 col-sm-3">
                                                <img src="<?= URL ?>img/produits_parquets/user.svg" class="img-user">
                                                <div class="review-block-name"><a href="#">VanBrulé</a></div>
                                                <div class="review-block-date">05 mai 2016<br/>Il y a 1 an</div>
                                            </div>
                                            <div class="col-lg-10 col-sm-9 review">
                                                <div class="review-block-rate">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <div class="review-block-title">Belle peinture mais pas donnée</div>
                                                <div class="review-block-description">
                                                   Bon achat Belle peinture bel effet facile a poser sans odeur mais un peu trop cher
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </main>
        </div>
        <?php require "../pages/footer.html"; ?>
        <!-- FIXED HEADER -->

        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function(){
                var offset = $(".menu").offset().top;
                $(document).scroll(function(){
                    var scrollTop = $(document).scrollTop();
                    if(scrollTop > offset){
                        $(".menu").addClass('fixed-top');
                    }
                    else {
                        $(".menu").removeClass('fixed-top');
                    }
                });
            });
        </script>
        <!--CAROUSEL-->
        <script>
            $(document).ready(function () {
                $('.thumbnail').click( function (e) {
                    e.preventDefault();
                    var source = $(this).attr('href');
                    $('#principale').attr({'src' : source});
                    $('.zoom').zoom();
                });
            });
        </script>
        <!--ZOOM IMG-->

        <script src='../js/jquery.zoom.js'></script>
        <script>
            $(document).ready(function(){
                $('.zoom').zoom();
            });
        </script>

    </body>
</html>

