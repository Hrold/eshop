<?php require_once("init.inc.php") ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no">
        <title>ESHOP</title>
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,500,500i,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link href="../css/style.css" rel="stylesheet" type="text/css">
        <link href="../css/style_cgv.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <?php require "../pages/header.html"; ?>
            <!--MAIN-->
            <main class="container">
                <div class="row">
                    <!--FIL ARIANE-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a title="homepage eshop" href="../index.php">Accueil</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Conditions générales de vente</li>
                        </ol>
                    </nav>
                </div>
                <div class="row">

                    <div class="col-8 offset-2">

                        <h3>Conditions générales de vente</h3>
                        <div id="accordion">
                            <div class="card-cgv">
                                <div class="card-header-cgv" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-cgv-One" aria-expanded="true" aria-controls="collapse-cgv-One">
                                            Article 1 : Objet et dispositions générales
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapse-cgv-One" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <p class="espace">Les présentes Conditions Générales de Vente déterminent les droits et obligations des partiesdans le cadre de la vente en ligne de Produits proposés par le Vendeur.Les présentes Conditions Générales de Vente (CGV) s’appliquent à toutes les ventes de Produits,effectuées au travers des sites Internet de la Société qui sont partie intégrante du Contrat entre l’Acheteur et le Vendeur.</p>
                                        <p class='espace'>Le Vendeur se réserve la possibilité de modifier les présentes, à toutmoment par la publication d’une nouvelle version sur son site Internet. Les CGV applicables alors sont celles étant en vigueur à la date du paiement (ou du premier paiement en cas de paiementsmultiples) de la commande.</p>
                                        <p class='espace'>La Société s’assure également que leur acceptation soit claire et sans réserve en mettant en place une case à cocher et un clic de validation. Le Client déclare avoir prisconnaissance de l’ensemble des présentes Conditions Générales de Vente, et le cas échéant desConditions Particulières de Vente liées à un produit ou à un service, et les accepter sans restriction ni réserve.</p>
                                        <p class='espace'>Le Client reconnaît qu’il a bénéficié des conseils et informations nécessaires afin des’assurer de l’adéquation de l’offre à ses besoins.</p> 
                                        <p class='espace'>Le Client déclare être en mesure de contracter légalement en vertu des lois françaises ou valablement représenter la personne physique oumorale pour laquelle il s’engage.</p>
                                        <p class='espace'>Sauf preuve contraire les informations enregistrées par la Société constituent la preuve de l’ensemble des transactions.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-cgv">
                                <div class="card-header-cgv" id="headingTwo">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-cgv-Two" aria-expanded="false" aria-controls="collapse-cgv-Two">
                                            Article 2 : Prix
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapse-cgv-Two" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        <p class='espace'>Les prix des produits vendus au travers des sites Internet sont indiqués en Euros toutes taxes. Pour tous les produits expédiés hors Union européenne et/ou DOM-TOM, le prix est calculé hors taxes automatiquement sur la facture. </p>
                                        <p class='espace'>Des droits de douane ou autres taxes locales ou droits d'importation ou taxes d'état sont susceptibles d'être exigibles dans certains cas. Ces droits et sommes ne relèvent pas du ressort du Vendeur. Ils seront à la charge de l'acheteur et relèvent de sa responsabilité (déclarations, paiement aux autorités compétentes, etc.). </p>
                                        <p class='espace'>Le Vendeur invite à ce titre l'acheteur à se renseigner sur ces aspects auprès des autorités locales correspondantes.</p>
                                        <p class='espace'>La Société se réserve la possibilité de modifier ses prix à tout moment pour l’avenir.</p>
                                        <p class='espace'>Les frais de télécommunication nécessaires à l’accès aux sites Internet de la Société sont à la charge du Client. Le cas échéant également, les frais de livraison.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-cgv">
                                <div class="card-header-cgv" id="headingThree">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-cgv-Three" aria-expanded="false" aria-controls="collapse-cgv-Three">
                                            Article 3 : Conclusion du contrat en ligne
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapse-cgv-Three" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        <p class='espace'>Le Client devra suivre une série d’étapes spécifiques à chaque Produit offert par le Vendeur pour pouvoir réaliser sa commande. Toutefois, les étapes décrites ci-après sont systématiques :</p> 
                                        <P class='espace'>- Information sur les caractéristiques essentielles du Produit ; </p>
                                        <p class='espace'>- Choix du Produit, le cas échéant de ses options et indication des données essentielles du Client (identification, adresse…) ; </p>
                                        <p class='espace'>- Acceptation des présentes Conditions Générales de Vente.</p>
                                        <p class='espace'>- Vérification des éléments de la commande et, le cas échéant, correction des erreurs. </p>
                                        <p class='espace'>- Suivi des instructions pour le paiement, et paiement des produits. </p>
                                        <p class='espace'>- Livraison des produits. Le Client recevra alors confirmation par courrier électronique du paiement de la commande, ainsi qu’un accusé de réception de la commande la confirmant. Il recevra un exemplaire .pdf des présentes conditions générales de vente. Pour lesproduits livrés, cette livraison se fera à l’adresse indiquée par le Client.Aux fins de bonne réalisation de la commande, et conformément à l’article 1316-1 du Code civil, le Client s’engage à fournir ses éléments d’identification véridiques.</p>
                                        <p class='espace'>Le Vendeur se réserve la possibilité de refuser la commande, par exemple pour toute demande anormale, réalisée de mauvaise foi ou pour tout motif légitime.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-cgv">
                                <div class="card-header-cgv" id="headingFour">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-cgv-Four" aria-expanded="false" aria-controls="collapse-cgv-Four">
                                            Article 4 : Produits et services
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapse-cgv-Four" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                    <div class="card-body">
                                        <p class='espace'>Les caractéristiques essentielles des biens, des services et leurs prix respectifs sont mis à disposition de l’acheteur sur les sites Internet de la société. </p>
                                        <p class='espace'>Le client atteste avoir reçu un détail des frais de livraison ainsi que les modalités de paiement, de livraison et d’exécution du contrat.</p>
                                        <p class='espace'>Le Vendeur s’engage à honorer la commande du Client dans la limite des stocks de Produits disponibles uniquement. A défaut, le Vendeur en informe le Client. </p>
                                        <p class='espace'>Ces informations contractuelles sont présentées en détail et en langue française. Conformément à la loi française, elles font l’objet d’un récapitulatif et d’une confirmation lors de la validation de la commande. </p>
                                        <p class='espace'>Les parties conviennent que les illustrations ou photos des produits offerts à la vente n’ont pas de valeurcontractuelle. La durée de validité de l’offre des Produits ainsi que leurs prix est précisée sur les sites Internet de la Société, ainsi que la durée minimale des contrats proposés lorsque ceux-ciportent sur une fourniture continue ou périodique de produits ou services. Sauf conditionsparticulières, les droits concédés au titre des présentes le sont uniquement à la personne physiquesignataire de la commande (ou la personne titulaire de l’adresse emailcommuniqué). Conformément aux dispositions légales en matière de conformité et de vices cachés, le Vendeur rembourse ou échange les produits défectueux ou ne correspondant pas à lacommande.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-cgv">
                                <div class="card-header-cgv" id="headingFive">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-cgv-Five" aria-expanded="false" aria-controls="collapse-cgv-Five">
                                            Article 5 : Clause de réserve de propriété
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapse-cgv-Five" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                                    <div class="card-body">
                                        <p class="espace">Les produits demeurent la propriété de la Société jusqu’au complet paiement du prix.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-cgv">
                                <div class="card-header-cgv" id="headingSix">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-cgv-Six" aria-expanded="false" aria-controls="collapse-cgv-Six">
                                            Article 6 : Modalités de livraison
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapse-cgv-Six" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                                    <div class="card-body">
                                        <p class="espace">Les produits sont livrés à l'adresse de livraison qui a été indiquée lors de la commande et le délai indiqué. Ce délai ne prend pas en compte le délai de préparation de la commande. Lorsque le Client commande plusieurs produits en même temps ceux-ci peuvent avoir des délais de livraison différents acheminés.</p>
                                        <p class="espace">En cas de retard d’expédition ou en cas de retard de livraison, le Client dispose de la possibilité de résoudre le contrat dans les conditions et modalités définies à l’Article L 138-2 du Code de la consommation.Le Vendeur procède alors au remboursement du produit et aux frais « aller » dans les conditions de l’Article L 138-3 du Code de la consommation.</p>
                                        <p class="espace">Le Vendeur met à disposition un point de contact téléphonique (coût d’une communication locale à partir d’un poste fixe) indiqué dans l’email de confirmation de commande afin d'assurer le suivi de la commande.</p>
                                        <p class="espace">Le Vendeur rappelle qu’au moment où le Client pend possession physiquement des produits, les risques de perte ou d’endommagement des produits lui est transféré. Il appartient au Client de notifier au transporteur toute réserves sur le produit livré.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-cgv">
                                <div class="card-header-cgv" id="headingSeven">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-cgv-Seven" aria-expanded="false" aria-controls="collapse-cgv-Seven">
                                            Article 7 : Disponibilité et présentation
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapse-cgv-Seven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                                    <div class="card-body">
                                        <p class='espace'>Les commandes seront traitées dans la limite de nos stocks disponibles ou sous réserve des stocks disponibles chez nos fournisseurs. En cas d’indisponibilité d’un article pour une période supérieure à 15 jours ouvrables, vous serez immédiatement prévenu des délais prévisibles de livraison et la commande de cet article pourra être annulée sur simple demande. Le Client pourra alors demander un avoir pour le montant de l’article ou son remboursement.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-cgv">
                                <div class="card-header-cgv" id="headingHeight">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-cgv-Height" aria-expanded="false" aria-controls="collapse-cgv-Height">
                                            Article 8 : Paiement
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapse-cgv-Height" class="collapse" aria-labelledby="headingHeight" data-parent="#accordion">
                                    <div class="card-body">
                                        <p class="espace">Le paiement est exigible immédiatement à la commande, y compris pour les produits en précommande. Le Client peut effectuer le règlement par carte de paiement ou chèque bancaire.</p>
                                        <p class="espace">Les cartes émises par des banques domiciliées hors de France doivent obligatoirement être des cartes bancaires internationales (Mastercard ou Visa). Le paiement sécurisé en ligne par carte bancaire est réalisé par notre prestataire de paiement. Les informations transmises sont chiffrées dans les règles de l’art et ne peuvent être lues au cours du transport sur le réseau Visa.</p>
                                        <p class='espace'>Une fois le paiement lancé par le Client, la transaction est immédiatement débitée après vérification des informations. Conformément à l’article L. 132-2 du Code monétaire et financier, l’engagement de payer donné par carte est irrévocable.</p>
                                        <p class="espace">En communiquant ses informations bancaires lors de la vente, le Client autorise le Vendeur à débiter sa carte du montant relatif au prix indiqué. Le Client confirme qu’il est bien le titulaire légal de la carte à débiter et qu’il est légalement en droit d’en faire usage.</p>
                                        <p class='espace'>En cas d’erreur, ou d’impossibilité de débiter la carte, la Vente est immédiatement résolue de plein droit et la commande annulée.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-cgv">
                                <div class="card-header-cgv" id="headingNine">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-cgv-Nine" aria-expanded="false" aria-controls="collapse-cgv-Nine">
                                            Article 9 : Délai de rétractation
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapse-cgv-Nine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                                    <div class="card-body">
                                        <p class='espace'>Conformément à l’article L. 121-20 du Code de la consommation, « le consommateur dispose d’un délai de quatorze jours euros pour exercer son droit de rétractation sans avoir à justifier de motifs ni à payer de pénalités, à l’exception, le cas échéant, des frais de retour ».</p>
                                        <p class='espace'>« Le délai mentionné à l’alinéa précédent court à compter de la réception pour les biens ou de l’acceptation de l’offre pour les prestations de services ».</p>
                                        <p class='espace'>En cas d’exercice du droit de rétractation dans le délai susmentionné, seul le prix du ou des produits achetés et les frais d’envoi seront remboursés, les frais de retour restent à la charge du Client.</p>
                                        <p class="espace">Les retours des produits sont à effectuer dans leur état d'origine et complets (emballage, accessoires, notice...) de sorte qu'ils puissent être recommercialisés à l’état neuf ; ils doivent si possible être accompagnés d’une copie du justificatif d'achat.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-cgv">
                                <div class="card-header-cgv" id="headingTen">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-cgv-Ten" aria-expanded="false" aria-controls="collapse-cgv-Ten">
                                            Article 10 : Garanties
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapse-cgv-Ten" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                                    <div class="card-body">
                                        <p class='espace'>Conformément à la loi, le Vendeur assume deux garanties : de conformité et relative aux vicescachés des produits. Le Vendeur rembourse l'acheteur ou échange les produits apparemment défectueux ou ne correspondant pas à la commande effectuée.</p>
                                        <p class='espace'>Le Vendeur rappelle que le consommateur :</p>
                                        <p class='espace'>- dispose d'un délai de 2 ans à compter de la délivrance du bien pour agirauprès du Vendeur</p>
                                        <p class="espace">- qu'il peut choisir entre le remplacement et la réparation du bien sous réserve des conditions prévues par l'art. apparemment défectueux ou ne correspondant</p>
                                        <p class="espace">- qu'il est dispensé d'apporter la preuve l’existence du défaut de conformité du bien durant les six mois suivant la délivrance du bien.</p>
                                        <p class="espace">- que, sauf biens d’occasion, ce délai sera porté à 24 mois à compter du 18 mars 2016</p>
                                        <p class="espace">- que le consommateur peut également faire valoir la garantie contre les vices cachés de la chose vendue au sens de l’article 1641 du code civil et, dans cette hypothèse, il peut choisir entre la résolution de la vente ou une réduction du prix de vente (dispositions des articles 1644 du Code Civil).</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-cgv">
                                <div class="card-header-cgv" id="headingEleven">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-cgv-Eleven" aria-expanded="false" aria-controls="collapse-cgv-Eleven">
                                            Article 11 : Réclamations
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapse-cgv-Eleven" class="collapse" aria-labelledby="headingEleven" data-parent="#accordion">
                                    <div class="card-body">
                                        <p class='espace'>Le cas échéant, l’Acheteur peut présenter toute réclamation en contactant la société au moyen du numéro de téléphone suivant : 01 28 02 29 45 (appel gratuit).</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-cgv">
                                <div class="card-header-cgv" id="headingTwelve">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-cgv-Twelve" aria-expanded="false" aria-controls="collapse-cgv-Twelve">
                                            Article 12 : Droits de propriété intellectuelle
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapse-cgv-Twelve" class="collapse" aria-labelledby="headingTwelve" data-parent="#accordion">
                                    <div class="card-body">
                                        <p class='espace'>Les marques, noms de domaines, produits, logiciels, images, vidéos, textes ou plus généralement toute information objet de droits de propriété intellectuelle sont et restent la propriété exclusive du vendeur. Aucune cession de droits de propriété intellectuelle n’est réalisée au travers des présentes CGV.</p>
                                        <p class="espace">Toute reproduction totale ou partielle, modification ou utilisation de ces biens pour quelque motif que ce soit est strictement interdite.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-cgv">
                                <div class="card-header-cgv" id="headingThirteen">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-cgv-Thirteen" aria-expanded="false" aria-controls="collapse-cgv-Thirteen">
                                            Article 13 : Force majeure
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapse-cgv-Thirteen" class="collapse" aria-labelledby="headingThirteen" data-parent="#accordion">
                                    <div class="card-body">
                                        <p class='espace'>L’exécution des obligations du vendeur au terme des présentes est suspendue en cas de survenance d’un cas fortuit ou de force majeure qui en empêcherait l’exécution. Le vendeur avisera le client de la survenance d’un tel évènement dès que possible.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-cgv">
                                <div class="card-header-cgv" id="headingFourteen">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-cgv-Fourteen" aria-expanded="false" aria-controls="collapse-cgv-Fourteen">
                                            Article 14 : nullité et modification du contrat
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapse-cgv-Fourteen" class="collapse" aria-labelledby="headingFourteen" data-parent="#accordion">
                                    <div class="card-body">
                                        <p class='espace'>Si l’une des stipulations du présent contrat était annulée, cette nullité n’entraînerait pas la nullité des autres stipulations qui demeureront en vigueur entre les parties. Toute modification contractuelle n’est valable qu’après un accord écrit et signé des parties.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-cgv">
                                <div class="card-header-cgv" id="headingFifteen">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-cgv-Fifteen" aria-expanded="false" aria-controls="collapse-cgv-Fifteen">
                                            Article 15 : Protection des données personnelles
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapse-cgv-Fifteen" class="collapse" aria-labelledby="headingFifteen" data-parent="#accordion">
                                    <div class="card-body">
                                        <p class='espace'>Conformément à la Loi Informatique et Libertés du 6 janvier 1978, vous disposez des droits d’interrogation, d’accès, de modification, d’opposition et de rectification sur les données personnelles vous concernant.</p>
                                        <p class='espace'>En adhérant à ces conditions générales de vente, vous consentez à ce que nous collections et utilisions ces données pour la réalisation du présent contrat. En saisissant votre adresse email sur l’un des sites de notre réseau, vous recevrez des emails contenant des informations et des offres promotionnelles concernant des produits édités par la Société et de ses partenaires.</p>
                                        <p class='espace'>Vous pouvez vous désinscrire à tout instant. Il vous suffit pour cela de cliquer sur le lien présent à la fin de nos emails ou de contacter le responsable du traitement (la Société) par lettre RAR. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-cgv">
                                <div class="card-header-cgv" id="headingSixteen">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-cgv-Sixteen" aria-expanded="false" aria-controls="collapse-cgv-Sixteen">
                                            Article 16 : Clause limitative de responsabilité
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapse-cgv-Sixteen" class="collapse" aria-labelledby="headingSixteen" data-parent="#accordion">
                                    <div class="card-body">
                                        <p class='espace'>Il est stipulé une clause limitative de responsabilité du Vendeur pour la réalisation de la prestation à 1000 euros.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-cgv">
                                <div class="card-header-cgv" id="headingSeventeen">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-cgv-Seventeen" aria-expanded="false" aria-controls="collapse-cgv-Seventeen">
                                            Article 17 : Droit applicable
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapse-cgv-Seventeen" class="collapse" aria-labelledby="headingSeventeen" data-parent="#accordion">
                                    <div class="card-body">
                                        <p class='espace'>Toutes les clauses figurant dans les présentes conditions générales de vente, ainsi que toutes les opérations d’achat et de vente qui y sont visées, seront soumises au droit français..</p>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </main>
        </div>

        <?php require "../pages/footer.html"; ?>
        <!-- FIXED HEADER -->

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function(){
                var offset = $(".menu").offset().top;
                $(document).scroll(function(){
                    var scrollTop = $(document).scrollTop();
                    if(scrollTop > offset){
                        $(".menu").addClass('fixed-top');
                    }
                    else {
                        $(".menu").removeClass('fixed-top');
                    }
                });
            });

        </script>

    </body>
</html>