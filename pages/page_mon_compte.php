<?php require_once("init.inc.php") ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no">
        <title>ESHOP</title>
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,500,500i,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link href="../css/style.css" rel="stylesheet" type="text/css">
        <link href="../css/style_mon_compte.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <?php require "../pages/header.html"; ?>
            <!--MAIN-->            
            <main class="container">
                <div class="row">
                    <!--FIL ARIANE-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a title="homepage eshop" href="../index.php">Accueil</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Mon compte</li>
                        </ol>
                    </nav>
                </div>

                <!--NEW CODE-->
                <div class="row">
                    <div class="col-md-5 mx-auto">
                        <div class="flip">
                            <div class="card"> 
                                <div class="face front"> 
                                    <div class="panel panel-default">
                                        <form class="form-horizontal">
                                            <h3>Déjà client ?</h3>
                                            <span class="subtitle">Connectez-vous</span>
                                            <label>Adresse e-mail</label>
                                            <input class="form-control" type="email" required="" placeholder="E-mail"/>
                                            <label>Mot de passe</label>
                                            <input class="form-control" placeholder="Mot de passe"/>
                                            <p class="text-right"><a title="mot de passe" href="#" class="mdp">Mot de passe oublié</a></p>
                                            <button class="btn btn-primary btn-block">Se connecter</button>
                                            <p class="blue">Se connecter avec</p>
                                            <p><span class="fa"><a href="#" class="fa-facebook-f fa-lg"></a></span><span class="fa"><a href="#" class="fa-twitter fa-lg"></a></span></p>
                                            <hr>
                                            <p class="text-center">
                                                <a title="nouveau client" href="#" class="fliper-btn">Nouveau client ?</a>
                                            </p>
                                        </form>
                                    </div>


                                </div> 
                                <div class="face back"> 
                                    <div class="panel panel-default">
                                        <form class="form-horizontal">
                                            <h3>Créer un compte</h3>
                                            <span class="subtitle">Rejoignez-nous</span>
                                            <label>Adresse e-mail</label>
                                            <input class="form-control" type="email" required="" placeholder="E-mail"/>
                                            <label>Nom</label>
                                            <input class="form-control" type="text" required="" placeholder="Nom"/><label>Prénom</label>
                                            <input class="form-control" type="text" required="" placeholder="Prénom"/>
                                            <label>Mot de passe</label>
                                            <input class="form-control" type="text" required="" placeholder="Mot de passe"/>
                                            <input class="form-control" type="text" required="" placeholder="Confirmez le mot de passe"/>
                                            <button class="btn btn-primary btn-block">S'inscrire</button>
                                            <p class="text-center">
                                                <a title="deja client" href="#" class="fliper-btn">Déja chez nous ?</a>
                                            </p>
                                            <p class="text-left">
                                                <input type="checkbox" id="neswletter" name="color" />
                                                <label class="checkbox-label" for="newsletter">S'inscrire à la newsletter</label>
                                            </p>
                                        </form>
                                    </div>
                                </div>
                            </div>   
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <?php require "../pages/footer.html"; ?>

        <!-- FIXED HEADER -->

        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <!-- FLIP CARD -->
        <script>
            $('.fliper-btn').click(function(){
                $('.flip').find('.card').toggleClass('flipped');
            });
        </script>
        <script>
            $(document).ready(function(){
                var offset = $(".menu").offset().top;
                $(document).scroll(function(){
                    var scrollTop = $(document).scrollTop();
                    if(scrollTop > offset){
                        $(".menu").addClass('fixed-top');
                    }
                    else {
                        $(".menu").removeClass('fixed-top');
                    }
                });
            });
        </script>
    </body>
</html>