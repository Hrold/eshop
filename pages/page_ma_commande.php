<?php require_once("init.inc.php") ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no">
        <title>ESHOP</title>
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,500,500i,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link href="../css/style.css" rel="stylesheet" type="text/css">
        <link href="../css/style_ma_commande.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <?php require "../pages/header.html"; ?>
            <!--MAIN-->            
            <main class="container">
                <div class="row">
                    <!--FIL ARIANE-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a title="homepage eshop" href="../index.php">Accueil</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Ma commande</li>
                        </ol>
                    </nav>
                </div>

                <!--NEW CODE-->
                <div class="row">
                    <div class="col-md-4 order-md-2 mb-4">
                        <h3 class="d-flex justify-content-between align-items-center mb-4">
                            <span>Votre panier</span>
                            <span class="badge badge-secondary badge-pill">3</span>
                        </h3>
                        <ul class="list-group mb-3">
                            <li class="list-group-item d-flex justify-content-between">
                                <div>
                                    <h6 class="mb-1">Carreaux de ciment imitations</h6>
                                    <small class="text-muted">Réf :  5052931463145</small>
                                </div>
                                <span>19€</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between">
                                <div>
                                    <h6 class="mb-1">Piscine jacuzzi</h6>
                                    <small class="text-muted">Réf :  8672934463135</small>
                                </div>
                                <span>2€</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between">
                                <div>
                                    <h6 class="mb-1">Planche de bois</h6>
                                    <small class="text-muted">Réf :  3128934463411</small>
                                </div>
                                <span>534€</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between bg-light">
                                <div class="text-success">
                                    <h6 class="mb-1">Code promo</h6>
                                    <small>ESHOP4LIFE</small>
                                </div>
                                <span class="text-success">-530€</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between">
                                <span>Total (€)</span>
                                <strong>25€</strong>
                            </li>
                        </ul>

                        <form class="card p-2">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Promo code">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-secondary">Appliquer</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-8 order-md-1">
                        <h3 class="mb-4">Adresse de facturation</h3>
                        <form class="needs-validation" novalidate>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="prenom">Prénom</label>
                                    <input type="text" class="form-control" id="prenom" placeholder="" value="" required>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="nom">Nom</label>
                                    <input type="text" class="form-control" id="nom" placeholder="" value="" required>
                                </div>
                            </div>

                            <div class="mb-3">
                                <label for="email">Adresse e-mail</label>
                                <input type="email" class="form-control" id="email" placeholder="">
                            </div>

                            <div class="mb-3">
                                <label for="adresse">Adresse</label>
                                <input type="text" class="form-control" id="adresse" placeholder="" required>
                            </div>

                            <div class="mb-3">
                                <label for="adresse2">Adresse 2 <span class="text-muted">(Optionnel)</span></label>
                                <input type="text" class="form-control" id="adresse2" placeholder="">
                            </div>

                            <div class="row">
                                <div class="col-md-5 mb-3">
                                    <label for="country">Pays</label>
                                    <select class="custom-select d-block w-100" id="country" required>
                                        <option value="">Choisir...</option>
                                        <option>Allemagne</option>
                                        <option>Angleterre</option>
                                        <option>Belgique</option>
                                        <option>Espagne</option>
                                        <option>France</option>
                                        <option>Italie</option>
                                        <option>Krypton</option>
                                    </select>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="ville">Ville</label>
                                    <input type="text" class="form-control" id="ville" placeholder="" required>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label for="CP">Code Postal</label>
                                    <input type="text" class="form-control" id="CP" placeholder="" required>
                                </div>
                            </div>
                            <hr class="mb-4">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="same-address">
                                <label class="custom-control-label" for="same-address">Conserver l'adresse de facturation pour l'adresse de livraison</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="save-info">
                                <label class="custom-control-label" for="save-info">Sauvegarder cette information pour la prochaine fois</label>
                            </div>
                            <hr class="mb-4">

                            <h3 class="mb-3">Paiement</h3>

                            <div class="d-block my-3">
                                <div class="custom-control custom-radio">
                                    <input id="credit" name="paymentMethod" type="radio" class="custom-control-input" checked required>
                                    <label class="custom-control-label" for="credit">Carte de crédit</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input id="paypal" name="paymentMethod" type="radio" class="custom-control-input" required>
                                    <label class="custom-control-label" for="paypal">Paypal</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="cc-name">Nom du propriétaire de la carte</label>
                                    <input type="text" class="form-control" id="cc-name" placeholder="" required>
                                    <small class="text-muted">Nom complet</small>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="cc-number">Numéro de carte</label>
                                    <input type="text" class="form-control" id="cc-number" placeholder="" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 mb-3">
                                    <label for="cc-expiration">Date d'expiration</label>
                                    <input type="text" class="form-control" id="cc-expiration" placeholder="" required>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="cc-expiration">Cryptogramme visuel</label>
                                    <input type="text" class="form-control" id="cc-cvv" placeholder="" required>
                                </div>
                            </div>
                            <hr class="mb-4">
                            <button class="mb-3 btn btn-primary btn-lg btn-block" type="submit">Procéder au paiement</button>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="cgv">
                                <label class="custom-control-label" for="cgv">J'ai lu et j'accepte les <a href="" title="cgv">conditions générales de vente</a></label>
                            </div>
                        </form>
                    </div>
                </div>



            </main>
        </div>
        <?php require "../pages/footer.html"; ?>

        <!-- FIXED HEADER -->

        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function(){
                var offset = $(".menu").offset().top;
                $(document).scroll(function(){
                    var scrollTop = $(document).scrollTop();
                    if(scrollTop > offset){
                        $(".menu").addClass('fixed-top');
                    }
                    else {
                        $(".menu").removeClass('fixed-top');
                    }
                });
            });
        </script>
    </body>
</html>