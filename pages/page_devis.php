<?php require_once("init.inc.php") ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no">
        <title>ESHOP</title>
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,500,500i,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link href="../css/style.css" rel="stylesheet" type="text/css">
        <link href="../css/style_devis.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <?php require "../pages/header.html"; ?>
            <!--MAIN-->            
            <main class="container">
                <div class="row">
                    <!--FIL ARIANE-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a title="homepage eshop" href="../index.php">Accueil</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Devis personnalisé</li>
                        </ol>
                    </nav>
                </div>

                <!--NEW CODE-->
                <div class="row">
                    <div class="col-md-8 mx-auto head-devis">
                        <h3 class="title">Faire un devis personnalisé</h3>
                    </div>
                    <div class="col-md-8 mx-auto label-devis">
                        <span class="before-devis mb-4">Des projets de travaux ou de bricolage ? Vous souhaitez trouver un artisan professionnel fiable, qualifié et sérieux près de chez vous ? N’hésitez pas à solliciter le réseau <span class="corporate">Eshop.</span><br> Faites une demande de devis travaux gratuits. Il vous suffit de remplir le formulaire ci-dessous pour être contacté par un professionnel du bâtiment à proximité de chez vous. </span>
                    </div>
                    <div class="col-md-8 mx-auto form-devis">
                        <form class="needs-validation" novalidate>
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label for="type_projet">Type de projet</label>
                                    <select class="custom-select d-block w-100" id="raison" required="">
                                        <option value="0">Type de projet</option>
                                        <option value="1" data-value="1" >Carrelage</option>
                                        <option value="2" data-value="2" >Parquet</option>
                                        <option value="1" data-value="3" >Escalier en bois</option>
                                        <option value="2" data-value="4" >Dominer le monde</option>
                                        <option value="3" data-value="5" >Rénovation de parquet</option>
                                        <option value="3" data-value="6" >Piscine en dur</option>
                                        <option value="1" data-value="7" >Spa</option>
                                        <option value="4" data-value="8" >Construire l'Etoile de la Mort</option>
                                    </select>
                                </div>
                                <div class="col-md-12 form-group">                                    <label for="message">Décrivez votre projet</label>
                                    <textarea class="form-control" id="message" rows="3"></textarea>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="type_individu">Vous êtes</label>
                                    <select class="custom-select d-block w-100" id="type_individu" required="">
                                        <option value="0" data-value="0">Propriétaire</option>
                                        <option value="1" data-value="1" >Locataire</option>
                                        <option value="2" data-value="2" >Sous-locataire</option>
                                        <option value="3" data-value="3" >Sous-sous-locataire</option>
                                        <option value="42" data-value="4" >l'empereur Palpatine</option>

                                    </select>
                                </div> 
                                <div class="col-md-4 mb-3">
                                    <label for="type_logement">Type de logement</label>
                                    <select class="custom-select d-block w-100" id="type_logement" required="">
                                        <option value="0" data-value="0">Maison</option>
                                        <option value="1" data-value="1" >Appartement</option>
                                        <option value="2" data-value="2" >Locaux professionnels</option>
                                        <option value="3" data-value="3" >Château</option>
                                        <option value="42" data-value="4" >Sénat Galactique</option>

                                    </select>
                                </div>   
                                <div class="col-md-4 mb-3">
                                    <label for="delai">Délais de la réalisation</label>
                                    <select class="custom-select d-block w-100" id="delai" required="">
                                        <option value="0" data-value="0">Immédiat</option>
                                        <option value="1" data-value="1" >Sous un mois</option>
                                        <option value="2" data-value="2" >Inférieur à 3 mois</option>
                                        <option value="3" data-value="3" >Entre 3 mois et 6 mois</option>
                                        <option value="42" data-value="4" >Avant l'ordre 66</option>
                                    </select>
                                </div>
                            </div>
                            <hr class="mb-3">  
                            <div class="row">
                                <div claSs="col-md-12 mb-3">
                                    <h3 class="text-center">Vos coordonnées</h3>
                                </div>
                                <div class="col-md-12 mb-2">
                                    <span class="civilite-title">Civilité</span>
                                    <div class="custom-control custom-radio">
                                        <input id="credit" name="civilite" type="radio" class="custom-control-input" required="">
                                        <label class="custom-control-label" for="credit">Madame</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input id="monsieur" name="civilite" type="radio" class="custom-control-input" required="">
                                        <label class="custom-control-label" for="monsieur">Monsieur</label>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="nom">Nom</label>
                                    <input type="text" class="form-control" id="nom" placeholder="" value="" required="">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="prenom">Prénom</label>
                                    <input type="text" class="form-control" id="prenom" placeholder="" value="" required="">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="cp">Code postal</label>
                                    <input type="text" class="form-control" id="cp" placeholder="" value="" required="">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="ville">Ville</label>
                                    <input type="text" class="form-control" id="ville" placeholder="" value="" required="">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="email">E-mail</label>
                                    <input type="text" class="form-control" id="email" placeholder="" value="" required="">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="telephone">Téléphone</label>
                                    <input type="text" class="form-control" id="telephone" placeholder="" value="" required="">
                                </div>
                                <div class="col-md-12">
                                <button class="mb-3 btn btn-primary btn-lg btn-block valid-devis" type="submit">Valider<span class="text-muted"><br>(gratuit) sans engagement</span></button></div>
                            </div>
                        </form>
                    </div>
                </div>



            </main>
        </div>
        <?php require "../pages/footer.html"; ?>

        <!-- FIXED HEADER -->

        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function(){
                var offset = $(".menu").offset().top;
                $(document).scroll(function(){
                    var scrollTop = $(document).scrollTop();
                    if(scrollTop > offset){
                        $(".menu").addClass('fixed-top');
                    }
                    else {
                        $(".menu").removeClass('fixed-top');
                    }
                });
            });
        </script>
    </body>
</html>