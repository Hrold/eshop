<?php require_once("init.inc.php") ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no">
        <title>ESHOP</title>
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,500,500i,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link href="../css/style.css" rel="stylesheet" type="text/css">
        <link href="../css/style_page_catalogue.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <?php require "../pages/header.html"; ?>
            <!--MAIN-->            
            <main class="container">
                <div class="row">
                    <!--FIL ARIANE-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../index.php">Accueil</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Page catalogue sanitaires</li>
                        </ol>
                    </nav>
                </div>
                <!--NOS PRODUITS-->
                <section class="row ligne_produits">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 en-tete"><h3>SANITAIRES</h3>
                                <span class="desc-catalogue">
                                    Vous êtes plutôt douche dynamisante ou baignoire balnéo apaisante ? Avec ESHOP, votre salle de bains s’adapte à vos envies et à votre emploi du temps pour vous faciliter la vie. Avec des meubles et des vasques pour tous les goûts, des douches, WC et baignoires faciles à poser, des robinets, du carrelage et même les accessoires pratiques, vous trouverez tous les produits 
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <aside class="col-md-3 filtre">
                                <h4 class="filtre-aside">Filtrer par</h4>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapseRetrait" aria-expanded="false">
                                    Options de retrait</a>
                                </span>
                                <ul class="list-options collapse" id="collapseRetrait">
                                    <li>
                                        <input type="checkbox" id="magasin" name="magasin" />
                                        <label class="checkbox-label" for="magasin">Retrait en magasin</label></li>
                                    <li>
                                        <input type="checkbox" id="domicile" name="domicile" />
                                        <label class="checkbox-label" for="domicile">Livraison à domicile</label>
                                    </li>
                                </ul>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapseProduit" aria-expanded="false">
                                    Type de produit</a>
                                </span>
                                <ul class="list-options collapse" id="collapseProduit">
                                    <li>
                                        <input type="checkbox" id="baignoire" name="baignoire" />
                                        <label class="checkbox-label" for="baignoire">Baignoire</label>
									</li>
                                    <li>
                                        <input type="checkbox" id="douche" name="douche" />
                                        <label class="checkbox-label" for="douche">Douche</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="wc" name="wc" />
                                        <label class="checkbox-label" for="wc">WC</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="robinet" name="robinet" />
                                        <label class="checkbox-label" for="robinet">Robinet</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="vasques" name="vasques" />
                                        <label class="checkbox-label" for="vasques">Vasques</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="meubles" name="meubles" />
                                        <label class="checkbox-label" for="meubles">Meubles</label>
                                    </li>
                                </ul>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapseCouleur" aria-expanded="false">
                                    Couleur</a>
                                </span>
                                <ul class="list-options collapse" id="collapseCouleur">
                                    <li>
                                        <input type="checkbox" id="noir" name="noir" />
                                        <label class="checkbox-label" for="noir">Noir</label>
									</li>
                                    <li>
                                        <input type="checkbox" id="blanc" name="blanc" />
                                        <label class="checkbox-label" for="blanc">Blanc</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="rouge" name="rouge" />
                                        <label class="checkbox-label" for="rouge">Rouge</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="gris" name="gris" />
                                        <label class="checkbox-label" for="gris">Gris</label>
                                    </li>
                                </ul>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapsePrix" aria-expanded="false">
                                    Prix</a>
                                </span>
                                <ul class="list-options collapse" id="collapsePrix">
                                    <li>
                                        <input type="checkbox" id="20" name="20" />
                                        <label class="checkbox-label" for="20">20,00€ - 59,99€</label></li>
                                    <li>
                                        <input type="checkbox" id="60" name="60" />
                                        <label class="checkbox-label" for="60">60,00€ - 99,99€</label></li>
                                    <li>
                                    <li>
                                        <input type="checkbox" id="100" name="100" />
                                        <label class="checkbox-label" for="100">100,00€ - 499,99€</label></li>
                                    <li>
                                    <li>
                                        <input type="checkbox" id="500" name="500" />
                                        <label class="checkbox-label" for="500">500,00€ - 999.99€</label></li>
                                    <li>
									<li>
                                        <input type="checkbox" id="1000" name="1000" />
                                        <label class="checkbox-label" for="1000">1000,00€ - 5000€</label></li>
                                    <li>
                                </ul>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapseEval" aria-expanded="false">
                                    Evaluation</a>
                                </span>
                                <ul class="list-options collapse" id="collapseEval">
                                    <li>
                                        <input type="checkbox" id="blue" name="color" />
                                        <label class="checkbox-label" for="blue">5 <span class="fa fa-star checked"></span>
                                        </label></li>
                                    <li>
                                        <input type="checkbox" id="red" name="color" />
                                        <label class="checkbox-label" for="blue">4 <span class="fa fa-star checked"></span> ou plus
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="red" name="color" />
                                        <label class="checkbox-label" for="blue">3 <span class="fa fa-star checked"></span> ou plus
                                        </label>
                                    </li>
                                </ul>

                            </aside>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_sanitaires.php" title ="Pack WC suspendu bâti universel">
                                            <img src='<?= URL ?>img/catalogue_sanitaire/img1.jpg' alt ="Pack WC suspendu bâti universel">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Pack WC suspendu bâti universel</p>
                                                <p class='prix'>299 € TTC</p>
                                            </div>
                                        </div>
										<p><span>60 € d'économie</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_sanitaire.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_sanitaires.php" title ="Cabine de douche rectangulaire">
                                            <img src='<?= URL ?>img/catalogue_sanitaire/img2.jpg'  alt ="Cabine de douche rectangulaire">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Cabine de douche rectangulaire</p>
                                                <p class='prix'>749 € TTC</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_sanitaires.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_sanitaires.php" title ="Mitigeur thermostatique douche">
                                            <img src='<?= URL ?>img/catalogue_sanitaire/img3.jpg' alt ="Mitigeur thermostatique douche">
                                        </a>
                                       
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Mitigeur thermostatique douche</p>
                                                <p class='prix'>149,99 € TTC</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_sanitaires.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre offset-md-2 side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_exterieur.php" title ="Baignoire balnéo, Bora Bora">
                                            <img src='<?= URL ?>img/catalogue_sanitaire/img4.jpg' alt ="Baignoire balnéo, Bora Bora">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Baignoire balnéo, Bora Bora</p>
                                                <p class='prix'>1590 € TTC</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_sanitaires.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_sanitaires.php" title ="Plafonnier Icaria">
                                            <img src='<?= URL ?>img/catalogue_sanitaire/img5.jpg' alt ="Plafonnier Icaria">
                                        </a>
                                        <p><span>livraison offerte</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Plafonnier Icaria</p>
                                                <p class='prix'>119 € TTC</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_sanitaires.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_sanitaires.php" title ="Meuble vasque">
                                            <img src='<?= URL ?>img/catalogue_sanitaire/img6.jpg' alt ="Meuble vasque">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Meuble vasque rouge, Remix</p>
                                                <p class='prix'>409 €</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_sanitaires.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                </div>
                                <nav aria-label="Page navigation example" class="nav-pagination">
                                    <ul class="pagination">
                                        <li class="page-item">
                                            <a class="page-link" href="#" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                        </li>
                                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item">
                                            <a class="page-link" href="#" aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </section>

            </main>
        </div>
        <?php require "../pages/footer.html"; ?>
        <!-- FIXED HEADER -->
        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function(){
                var offset = $(".menu").offset().top;
                $(document).scroll(function(){
                    var scrollTop = $(document).scrollTop();
                    if(scrollTop > offset){
                        $(".menu").addClass('fixed-top');
                    }
                    else {
                        $(".menu").removeClass('fixed-top');
                    }
                });
            });
        </script>
        <!-- FILTER -->

    </body>
</html>