<?php require_once("init.inc.php") ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no">
        <title>ESHOP</title>
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,500,500i,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link href="../css/style.css" rel="stylesheet" type="text/css">
        <link href="../css/style_professionnels.css" rel="stylesheet" type="text/css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <style>
            #map {
                height: 400px;
                width: 100%;
            }
        </style>


    </head>
    <body>
        <div class="container-fluid">
            <?php require "../pages/header.html"; ?>
            <!--MAIN-->            
            <main class="container">
                <div class="row">
                    <!--FIL ARIANE-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a title="homepage eshop" href="../index.php">Accueil</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Professionnels</li>
                        </ol>
                    </nav>
                </div>

                <!--NEW CODE-->
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <h3>Découvrez nos offres pour les professionnels</h3>
                    </div>
                    <div class="col-md-3 categorie">
                        <a href="#" title="outillage electroportatif">
                            <span class="encart-pro"><img src="../img/professionnels/picto-img-1.png">Outillage électroportatif</span>
                            <span class="color"></span>
                            <img class="background-pro-img" src="../img/professionnels/img1.jpg">
                        </a>
                    </div>
                    <div class="col-md-4 offset-md-1 mb-4 categorie">
                        <a href="#" title="gros oeuvre">
                            <span class="encart-pro"><img src="../img/professionnels/picto-img-2.png">Gros oeuvre</span>
                            <span class="color"></span>
                            <img class="background-pro-img" src="../img/professionnels/img2.jpg">
                        </a>
                    </div>
                    <div class="col-md-3 offset-md-1 mb-4 categorie">
                        <a href="#" title="outillage à main">
                            <span class="encart-pro"><img src="../img/professionnels/picto-img-3.png">Outillage à main</span>
                            <span class="color"></span>
                            <img class="background-pro-img" src="../img/professionnels/img3.jpg">
                        </a>
                    </div>
                    <div class="col-md-4 categorie">
                        <a href="#" title="échafaudage et échelle">
                            <span class="encart-pro"><img src="../img/professionnels/picto-img-4.png">&Eacute;chafaudage / &Eacute;chelle</span>
                            <span class="color"></span>
                            <img class="background-pro-img" src="../img/professionnels/img4.jpg">
                        </a>
                    </div>
                    <div class="col-md-2 offset-md-1 categorie">
                        <a href="#" title="lasers et mesures">
                            <span class="encart-pro"><img src="../img/professionnels/picto-img-5.png">Lasers et mesures</span>
                            <span class="color"></span>
                            <img class="background-pro-img" src="../img/professionnels/img5.jpg">
                        </a>
                    </div>
                    <div class="col-md-4 offset-md-1 categorie">
                        <a href="#" title="outillage de chantier">
                            <span class="encart-pro"><img src="../img/professionnels/picto-img-6.png">Outillage de chantier</span>
                            <span class="color"></span>
                            <img class="background-pro-img" src="../img/professionnels/img6.jpg">
                        </a>
                    </div>
                </div>

            </main>
        </div>
        <?php require "../pages/footer.html"; ?>

        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <!-- FIXED HEADER -->

        <script>
            $(document).ready(function(){
                var offset = $(".menu").offset().top;
                $(document).scroll(function(){
                    var scrollTop = $(document).scrollTop();
                    if(scrollTop > offset){
                        $(".menu").addClass('fixed-top');
                    }
                    else {
                        $(".menu").removeClass('fixed-top');
                    }
                });
            });
        </script>
    </body>
</html>