<?php require_once("init.inc.php") ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no">
        <title>ESHOP</title>
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,500,500i,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link href="../css/style.css" rel="stylesheet" type="text/css">
        <link href="../css/style_mon_panier.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <?php require "../pages/header.html"; ?>
            <!--MAIN-->            
            <main class="container">
                <div class="row">
                    <!--FIL ARIANE-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a title="homepage eshop" href="../index.php">Accueil</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Mon panier</li>
                        </ol>
                    </nav>
                </div>

                <!--NEW CODE-->
                <div class="row">
                    <div class="col-12">
                        <h3 class="mb-3">Votre panier</h3>
                    </div>
                    <div class="col-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col"> </th>
                                        <th scope="col">Articles</th>
                                        <th scope="col">Disponibilité</th>
                                        <th scope="col" class="text-center quant-width">Quantité</th>
                                        <th scope="col" class="text-right">Prix</th>
                                        <th> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><img src="../img/catalogue_carrelages/carreauCiment1.jpg" /> </td>
                                        <td>Carreaux de ciment imitations<br><small class="text-muted"> Réf : 5052931463145 </small></td>
                                        <td>En stock</td>
                                        <td class="text-center">
                                            <p class="gestion-quant">
                                                <button class="btn btn-minus value-control" data-action="minus" data-target="prod-1">
                                                    <span class="fa fa-minus"></span>
                                                </button>
                                                <input type="text" value="1" class="form-control quantite" id="prod-1">
                                                <button class="btn btn-plus value-control" data-action="plus" data-target="prod-1">
                                                    <span class="fa fa-plus"></span>
                                                </button>
                                            </p>
                                        </td>
                                        <td class="text-right align-middle">124,90 €</td>
                                        <td class="text-center"><button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> </button> </td>
                                    </tr>
                                    <tr>
                                        <td><img src="../img/catalogue_carrelages/carreauCiment2.jpg" /> </td>
                                        <td>Piscine jacuzzi<br><small class="text-muted">Réf :  8672934463135</small></td>
                                        <td>En stock</td>
                                        <td class="text-center">
                                            <p class="gestion-quant">
                                                <button class="btn btn-minus value-control" data-action="minus" data-target="prod-2">
                                                    <span class="fa fa-minus"></span>
                                                </button>
                                                <input type="text" value="1" class="form-control quantite" id="prod-2">
                                                <button class="btn btn-plus value-control" data-action="plus" data-target="prod-2">
                                                    <span class="fa fa-plus"></span>
                                                </button>
                                            </p>
                                        </td>
                                        <td class="text-right align-middle">33,90 €</td>
                                        <td class="text-center"><button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> </button> </td>
                                    </tr>
                                    <tr>
                                        <td><img src="../img/catalogue_carrelages/carreauCiment3.jpg" /> </td>
                                        <td>Planche de bois<br><small class="text-muted">Réf :  8672934463135</small></td>
                                        <td>En stock</td>
                                        <td class="text-center">
                                            <p class="gestion-quant">
                                                <button class="btn btn-minus value-control" data-action="minus" data-target="prod-3">
                                                    <span class="fa fa-minus"></span>
                                                </button>
                                                <input type="text" value="1" class="form-control quantite" id="prod-3">
                                                <button class="btn btn-plus value-control" data-action="plus" data-target="prod-3">
                                                    <span class="fa fa-plus"></span>
                                                </button>
                                            </p>
                                        </td>
                                        <td class="text-right align-middle">70,00 €</td>
                                        <td class="text-center"><button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> </button> </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="calcul-label">Sous-Total</td>
                                        <td class="text-right calcul-label">255,90 €</td>
                                        <td></td>

                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="calcul-label">Frais de livraison</td>
                                        <td class="text-right calcul-label">6,90 €</td>
                                        <td></td>

                                    </tr>                                    <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-success calcul-label">Code de réduction<br><small class="text-muted">ESHOP4LIFE</small></td>
                                    <td class="text-right text-success calcul-label">-530 €</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="calcul-label">Total (€)</td>
                                        <td class="text-right calcul-label"><strong>346,90 €</strong></td>
                                        <td></td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col mb-2">
                        <div class="row">
                            <div class="col-sm-12  col-md-6">
                                <button class="btn btn-lg btn-block btn-light">Continuer vos  achats</button>
                            </div>
                            <div class="col-sm-12 col-md-6 text-right">
                                <button class="btn btn-lg btn-block btn-success">Finaliser la commande</button>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <?php require "../pages/footer.html"; ?>

        <!-- FIXED HEADER -->
        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <!-- MINUS / PLUS BUTTON -->
        <script>
            $(document).on('click','.value-control',function(){
                var action = $(this).attr('data-action')
                var target = $(this).attr('data-target')
                var value  = parseFloat($('[id="'+target+'"]').val());
                if ( action == "plus" ) {
                    value++;
                }
                if ( action == "minus" ) {
                    value--;
                }
                $('[id="'+target+'"]').val(value)
            })
        </script>
        <script>
            $(document).ready(function(){
                var offset = $(".menu").offset().top;
                $(document).scroll(function(){
                    var scrollTop = $(document).scrollTop();
                    if(scrollTop > offset){
                        $(".menu").addClass('fixed-top');
                    }
                    else {
                        $(".menu").removeClass('fixed-top');
                    }
                });
            });
        </script>
    </body>
</html>