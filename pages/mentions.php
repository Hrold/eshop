<?php require_once("init.inc.php") ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no">
        <title>ESHOP</title>
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,500,500i,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link href="../css/style.css" rel="stylesheet" type="text/css">
        <link href="../css/style_mentions.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <?php require "../pages/header.html"; ?>
            <!--MAIN-->            
            <main class="container">
                <div class="row">
                    <!--FIL ARIANE-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../index.php">Accueil</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Mentions légales</li>
                        </ol>
                    </nav>
                </div>
                <!--NOS PRODUITS-->
                <section class="row ligne_produits">
                    <div class="col-12">
                        <h3 class="title-section-produits">1. COORDONNÉES</h3>
						<hr>
						<p class='espace'>La société ESHOP (ci-après la "Société" ) est une société anonyme à conseil d'administration (SA) au capital de 100.000.000,00 euros, dont le siège social est Rue Leclerc, 75015 Paris immatriculée sous le numéro RCS Lille B 587 660 742.</p>
						<p class='espace'>Les sociétés SA ESHOP France & SNC GSB EHOP sont certifiées pour les activités de mise en vente, vente et de conseil à l'utilisation des produits phytopharmaceutiques, sous les numéros d'agrément suivants :</p> 
						<p class='espace'>- n° d'agrement SA ESHOP : NC05286</p>
						<p class='espace'>La société ESHOP est l'éditeur du site Internet ESHOP (ci-après le "Site" ).</p>
						<p class='espace'>Pour toute question, nous vous remercions de bien vouloir vous adresser au Service Clientèle de ESHOP :</p>
						<p class='espace'>Société ESHOP - Service Clientèle</p>
						<p class='espace'>Téléphone :  01 28 02 29 45</p>
						<p class='espace'>L'accès au Site ainsi que l'utilisation de son contenu s'effectue dans le cadre des mentions d'utilisation décrites ci-après (ci-après les "Mentions légales"). Le fait d'accéder et de naviguer sur le Site constitue de votre part une acceptation sans réserve des précisions suivantes.</p>
					</div>
				</section>
				<section class="row ligne_produits">
                    <div class="col-12">
						<h3 class="title-section-produits">2. DROITS DE PROPRIÉTÉ INTELLECTUELLE</h3>
						<hr>
						<p class='espace'>La Société est titulaire du nom de domaine www.eshop.com . Le Site est une oeuvre de l'esprit protégée par les lois de la propriété intellectuelle. Le Site dans son ensemble et chacun des éléments qui le composent (tels que textes, arborescences, logiciels, animations, photographies, illustrations, schémas, logos) sont la propriété exclusive de la Société qui est seule habilitée à utiliser les droits de propriété intellectuelle y afférents</p>
						<p class='espace'>L'usage de tout ou partie du Site, notamment par téléchargement, reproduction, transmission, représentation ou diffusion à d'autres fins que pour votre usage personnel et privé dans un but non commercial est strictement interdit. La violation de ces dispositions vous soumet aux sanctions prévues tant par le Code de la Propriété Intellectuelle et le Code Pénal au titre notamment de la contrefaçon de droit d'auteur et du droit des marques, que par le Code Civil en matière de responsabilité civile.</p>
						<p class='espace'>Les marques et noms de domaine ainsi que les dessins et modèles qui apparaissent sur le Site sont la propriété exclusive de la Société. La création de liens hypertextes vers le Site ne peut être faite qu'avec l'autorisation écrite et préalable de la Société, laquelle autorisation peut être révoquée à tout moment.</p>
						<p class='espace'>Tous les sites ayant un lien hypertexte vers le Site ne sont pas sous le contrôle de la Société et celle-ci décline par conséquent toute responsabilité (notamment éditoriale) concernant l'accès et le contenu de ces sites.</p>
						<p class='espace'>La Société est libre de modifier, à tout moment et sans préavis, le contenu du Site.</p>
					</div>
				</section>
				<section class="row ligne_produits">
                    <div class="col-12">
						<h3 class="title-section-produits">3. DONNÉES PERSONNELLES</h3>
						<hr>
						<p class='espace'>La Société dispose d'une politique de données personnelles pour son Site, conformément à la loi "Informatique et Libertés" du 6 janvier 1978 telle que modifiée par la loi du 6 août 2004. Vous pouvez la consulter en accédant à la section "Politique de données personnelles".</p>
						<p class='espace'>Pour consulter et visiter le Site, vous n'avez pas à communiquer de données personnelles.</p>
						<p class='espace'>La Société est responsable du traitement de vos données personnelles et en est la seule destinataire. La Société s'engage à préserver la confidentialité de vos données personnelles.</p>
						<p class='espace'>Conformément à la loi "Informatique et Libertés" du 6 janvier 1978 telle que modifiée par la loi du 6 août 2004, le Site a fait l'objet d'une déclaration auprès de la CNIL (récépissé n°745955) et vous disposez d'un droit d'accès, de modification et de suppression au traitement de vos données personnelles. Vous pouvez exercer vos droits en adressant un courrier à :</p>
						<p class='espace'>Société ESHOP - Service clientèle</p>
						<p class='espace'>Téléphone : 01 28 02 29 45</p>
					</div>
				</section>
				<section class="row ligne_produits">
                    <div class="col-12">
						<h3 class="title-section-produits">4. GARANTIES ET RESPONSABILITÉS</h3>
						<hr>
						<p class='espace'>La Société s'efforce d'assurer au mieux que les informations accessibles par l'intermédiaire du Site soient exactes et mises à jour. Cependant, la Société ne garantit en aucune manière que ces informations soient exactes, complètes et à jour. La Société n'assure aucune garantie, expresse ou tacite, concernant tout ou partie du Site. La Société décline également toute responsabilité concernant l'accès et le contenu de sites liés au Site.</p>
						<p class='espace'>En aucun cas, la Société ne peut être tenue pour responsable d'un quelconque dommage direct ou indirect, quelle qu'en soit la cause, origine, nature et conséquence, découlant de la consultation ou de l'utilisation du Site. Notamment, la Société décline toute responsabilité en cas d'interruption ou d'inaccessibilité du Site, de survenance de bogues, de tout dommage résultant d'acte frauduleux de tiers (tels que intrusion) à partir du Site.</p>
						<p class='espace'>Dans le cadre d'une volonté de proposer à ses clients une information complète dans le domaine des travaux et du bricolage, la Société met à la disposition des utilisateurs du Site des liens hypertextes permettant d'accéder à des sites internet tiers (ci-après les "Sites Cibles" ). La Société ne peut être tenue pour responsable du contenu des Sites Cibles, la Société n'exerçant aucun contrôle sur ces derniers. De même, la Société ne peut être tenue pour responsable des dysfonctionnements des Sites Cibles ni de l'endommagement des systèmes d'information de l'utilisateur lié à l'utilisation des Sites Cibles.</p>
						<p class='espace'>La Société met en oeuvre des moyens destinés à assurer la sécurité des fichiers constitués à partir des données à caractère personnel collectées sur le Site. La Société ne maîtrise pas les risques liés au fonctionnement d'Internet et attire votre attention sur l'existence d'éventuels risques en terme de confidentialité des données transitant via ce réseau.</p>
					</div>
				</section>
				<section class="row ligne_produits">
                    <div class="col-12">
						<h3 class="title-section-produits">5. MODIFICATION DES MENTIONS LÉGALES</h3>
						<hr>
						<p class='espace'>La Société vous informe que les Mentions Légales peuvent être modifiées à tout moment. Ces modifications sont publiées par leur mise en ligne et sont réputées acceptées sans réserve lorsque vous accédez au Site postérieurement à leur mise en ligne. Nous vous recommandons de consulter régulièrement la présente page.</p>
					</div>
				</section>
				<section class="row ligne_produits">
                    <div class="col-12">
						<h3 class="title-section-produits">6. LITIGES</h3>
						<hr>
						<p class='espace'>Les Mentions Légales sont établies en conformité avec le droit français et notamment les dispositions de la loi du 21 juin 2004 pour la confiance dans l'économie numérique ainsi que la loi du 6 janvier 1978 modifiée par la loi du 6 août 2004 Informatique et Libertés. </p>
						<p class='espace'>Les juridictions de Lille (France) sont territorialement compétentes pour connaître de tout litige afférent au Site. </p>
					</div>
				</section>
            </main>
        </div>
        <?php require "../pages/footer.html"; ?>
        <!-- FIXED HEADER -->
        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function(){
                var offset = $(".menu").offset().top;
                $(document).scroll(function(){
                    var scrollTop = $(document).scrollTop();
                    if(scrollTop > offset){
                        $(".menu").addClass('fixed-top');
                    }
                    else {
                        $(".menu").removeClass('fixed-top');
                    }
                });
            });
        </script>
        <!-- FILTER -->

    </body>
</html>