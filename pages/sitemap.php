<?php require_once("init.inc.php") ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no">
        <title>Plan du site</title>
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,500,500i,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link href="<?= URL ?>css/style.css" rel="stylesheet" type="text/css">
        <link href="<?= URL ?>css/style_sitemap.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <?php require "../pages/header.html"; ?>
            <!--MAIN-->
            <main class="container">
                <div class="row">
                    <!--FIL ARIANE-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a title="homepage eshop" href="<?= URL ?>index.php">Accueil</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Plan du site</li>
                        </ol>
                    </nav>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p class="presentation"><a href="<?= URL ?>index.php" title="Accueil">Accueil</a> </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <section>
                            <h2 class="map-title mb-3">Catégorie</h2>
                            <div class="row">
                                <div class="col-md-3">
                                    <ul class="list-cate">
                                        <li><a href="<?= URL ?>pages/page_catalogue_carrelage.php" title="Catalogue carrelage">Carrelage</a></li>
                                        <li><a href="<?= URL ?>pages/page_catalogue_plancher.php" title="Catalogue parquet">Parquet</a></li>
                                        <li><a href="<?= URL ?>pages/page_catalogue_exterieur.php" title="Catalogue extérieur">Extérieur</a></li>
                                        <li><a href="<?= URL ?>pages/page_catalogue_mur.php" title="Catalogue mur">Mur</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-3">
                                    <ul class="list-cate">
                                        <li><a href="<?= URL ?>pages/page_catalogue_accessoires.php" title="Catalogue accessoires">Accessoires</a></li>
                                        <li><a href="<?= URL ?>pages/page_pose.php" title="Pose">Pose</a></li>
                                        <li><a href="<?= URL ?>pages/page_catalogue_sanitaire.php" title="Catalogue sanitaires">Sanitaires</a></li>
                                    </ul>
                                </div>
                            </div>
                        </section>
                        <section>
                            <h2 class="map-title mb-3">Page produit</h2>
                            <div class="row">
                                <div class="col-md-3">
                                    <ul class="list-cate">
                                        <li><a href="<?= URL ?>pages/page_produit_carrelage.php" title="Page produit carrelage">Carrelage</a></li>
                                        <li><a href="<?= URL ?>pages/page_produit_plancher.php" title="Page produit parquet">Parquet</a></li>
                                        <li><a href="<?= URL ?>pages/page_produit_exterieur.php" title="Page produit éxtérieur">Extérieur</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-3">
                                    <ul class="list-cate">
                                        <li><a href="<?= URL ?>pages/page_produit_mur.php" title="Page produit mur">Mur</a></li>
                                        <li><a href="<?= URL ?>pages/page_produit_accessoire.php" title="Page produit accessoires">Accessoires</a></li>
                                        <li><a href="<?= URL ?>pages/page_produit_sanitaires.php" title="Page produit sanitaires">Sanitaires</a></li>
                                    </ul>
                                </div>
                            </div>
                        </section>
                        <section>
                            <h2 class="map-title mb-3">Options</h2>
                            <div class="row">
                                <div class="col-md-3">
                                    <ul class="list-cate">
                                        <li><a href="<?= URL ?>pages/page_devis.php"title="Devis personnalisé">Devis personnalisé</a></li>
                                        <li><a href="<?= URL ?>pages/page_mon_compte.php" title="Mon compte">Mon compte</a></li>
                                        <li><a href="<?= URL ?>">Conseils et questions</a></li>
                                        <li><a href="<?= URL ?>pages/page_mon_panier.php" title="Mon panier">Mon panier</a></li>
                                    </ul>
                                </div>
                            </div>
                        </section>
                        <section>
                            <h2 class="map-title mb-3">Liens divers</h2>
                            <div class="row">
                                <div class="col-md-3">
                                    <ul class="list-cate">
                                        <li><a href="<?= URL ?>pages/page_qui_sommes_nous.php" title="Qui sommes-nous ?">Qui sommes-nous ?</a></li>
                                        <li><a href="<?= URL ?>pages/page_catalogue_produits.php" title="Page catalogue produits">Nos produits</a></li>
                                        <li><a href="<?= URL ?>pages/page_boutique.php" title="Nos boutiques">Nos boutiques</a></li>
                                        <li><a href="<?= URL ?>pages/page_devis.php">Demande de devis</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-3">
                                    <ul class="list-cate">
                                        <li><a href="<?= URL ?>pages/page_professionnels.php">Contact</a></li>
                                        <li><a href="<?= URL ?>">Professionnel</a></li>
                                    </ul>
                                </div>
                            </div>
                        </section>
                        <section>
                            <h2 class="map-title mb-3">Informations</h2>
                            <div class="row">
                                <div class="col-md-3">
                                    <ul class="list-cate">
                                        <li><a href="<?= URL ?>pages/page_ma_commande.php">Votre commande</a></li>
                                        <li><a href="<?= URL ?>pages/delai.php" title="Délais et frais de livraison">Délais et frais de livraisons</a></li>
                                        <li><a href="<?= URL ?>pages/paiement.php" title="Sécurité de paiement">Sécurité de paiement</a></li>
                                        <li><a href="<?= URL ?>pages/retour.php" title="Echange et retour">&Eacute;changes et retours express</a></li>
                                        <li><a href="<?= URL ?>pages/parrainage.php" title="Parrainage">Parrainage</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <ul class="list-cate">
                                        <li><a href="<?= URL ?>pages/cadeau.php" title="Carte cadeau">Carte cadeau</a></li>
                                        <li><a href="<?= URL ?>pages/fidelite.php" title="Point de fidélité">Points de fidélité</a></li>
                                        <li><a href="<?= URL ?>pages/avoir.php" title="Avoirs et remises">Avoirs et remises</a></li>
                                        <li><a href="<?= URL ?>pages/cgv.php" title="Conditions générales de vente">Conditions générales de vente</a></li>
                                        <li><a href="<?= URL ?>pages/mentions.php" title="Mentions légales">Mentions légales</a></li>
                                    </ul>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <!-- /container -->
            </main>
        </div>
        <?php require "../pages/footer.html"; ?>
        <!-- FIXED HEADER -->
        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">-->
        <script src="../js/stylemap.js"></script>
        <script>
            $(document).ready(function(){
                var offset = $(".menu").offset().top;
                $(document).scroll(function(){
                    var scrollTop = $(document).scrollTop();
                    if(scrollTop > offset){
                        $(".menu").addClass('fixed-top');
                    }
                    else {
                        $(".menu").removeClass('fixed-top');
                    }
                });
            });
        </script>
        <!-- FILTER -->

    </body>
</html>