<?php require_once("init.inc.php") ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no">
        <title>ESHOP</title>
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,500,500i,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link href="../css/style.css" rel="stylesheet" type="text/css">
        <link href="../css/style_page_catalogue.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <?php require "../pages/header.html"; ?>
            <!--MAIN-->            
            <main class="container">
                <div class="row">
                    <!--FIL ARIANE-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= URL ?>index.php"title="Accueil">Accueil</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Page catalogue carrelage</li>
                        </ol>
                    </nav>
                </div>
                <!--NOS PRODUITS-->
                <section class="row ligne_produits">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 en-tete"><h3>TOUS NOS PRODUITS</h3>
                                <span class="desc-catalogue">
                                    Dans cette rubrique vous découvrirez l'ensemble du catalogue de notre boutique ESHOP. Tout y est rangé méticuleusement, n'hésitez pas à utiliser nos filtres de recherche pour affiner vos résultats. Certains produits ne sont disponible qu'en magasin, il suffit de les réserver sur notre site, de sélectionner votre magasin le plus proche et vous pourrez les récupérer sur place seulement quelques heures après.
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <aside class="col-md-3 filtre">
                                <h4 class="filtre-aside">Filtrer par</h4>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapseRetrait" aria-expanded="false">
                                    Options de retrait</a>
                                </span>
                                <ul class="list-options collapse" id="collapseRetrait">
                                    <li>
                                        <input type="checkbox" id="magasin" name="magasin" />
                                        <label class="checkbox-label" for="magasin">Retrait en magasin</label></li>
                                    <li>
                                        <input type="checkbox" id="domicile" name="domicile" />
                                        <label class="checkbox-label" for="domicile">Livraison à domicile</label>
                                    </li>
                                </ul>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapseProduit" aria-expanded="false">
                                    Type de produit</a>
                                </span>
                                <ul class="list-options collapse" id="collapseProduit">
                                    <li>
                                        <input type="checkbox" id="carrelage" name="carrelage" />
                                        <label class="checkbox-label" for="carrelage">Carrelage</label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="parquet" name="parquet" />
                                        <label class="checkbox-label" for="parquet">Parquet</label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="exterieur" name="exterieur" />
                                        <label class="checkbox-label" for="exterieur">Extérieur</label>
									</li>
                                    <li>
                                        <input type="checkbox" id="mur" name="mur" />
                                        <label class="checkbox-label" for="mur">Mur</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="accessoires" name="accessoires" />
                                        <label class="checkbox-label" for="accessoires">Accessoires</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="sanitaires" name="sanitaires" />
                                        <label class="checkbox-label" for="sanitaires">Sanitaires</label>
                                    </li>
                                </ul>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapseMatiere" aria-expanded="false">
                                    Matière</a>
                                </span>
                                <ul class="list-options collapse" id="collapseMatiere">
                                    <li>
                                        <input type="checkbox" id="gres-teinte" name="gres-teinte" />
                                        <label class="checkbox-label" for="gres-teinte">Grès cérame émaillé teinté</label></li>
                                    <li>
                                        <input type="checkbox" id="gres-masse" name="gres-masse" />
                                        <label class="checkbox-label" for="gres-masse">Grès cérame pleine masse</label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="chene" name="chene" />
                                        <label class="checkbox-label" for="chene">Chêne</label></li>
                                    <li>
                                        <input type="checkbox" id="massif" name="massif" />
                                        <label class="checkbox-label" for="massif">Massif</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="contrecolle" name="contrecolle" />
                                        <label class="checkbox-label" for="contrecolle">Contre collé</label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="bois" name="bois" />
                                        <label class="checkbox-label" for="bois">Bois</label>
									</li>
                                    <li>
                                        <input type="checkbox" id="teck" name="teck" />
                                        <label class="checkbox-label" for="teck">Teck</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="metal" name="metal" />
                                        <label class="checkbox-label" for="metal">Métal</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="plastique" name="plastique" />
                                        <label class="checkbox-label" for="plastique">Plastique</label>
                                    </li>
                                </ul>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapseCouleur" aria-expanded="false">
                                    Couleur</a>
                                </span>
                                <ul class="list-options collapse" id="collapseCouleur">
                                    <li>
                                        <input type="checkbox" id="brun" name="brun" />
                                        <label class="checkbox-label" for="brun">Brun</label></li>
                                    <li>
                                        <input type="checkbox" id="gris" name="gris" />
                                        <label class="checkbox-label" for="gris">Gris</label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="blanc" name="blanc" />
                                        <label class="checkbox-label" for="blanc">Blanc</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="noir" name="noir" />
                                        <label class="checkbox-label" for="noir">Noir</label>
                                    </li>
									<li>
                                        <input type="checkbox" id="rouge" name="rouge" />
                                        <label class="checkbox-label" for="rouge">Rouge</label>
                                    </li>

                                </ul>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapsePrix" aria-expanded="false">
                                    Prix</a>
                                </span>
                                <ul class="list-options collapse" id="collapsePrix">
                                    <li>
                                        <input type="checkbox" id="1" name="1" />
                                        <label class="checkbox-label" for="1">1,00€ - 10,00€</label></li>
                                    <li>
                                    <li>
                                        <input type="checkbox" id="10" name="10" />
                                        <label class="checkbox-label" for="10">10,00€ - 25,00€</label></li>
                                    <li>
                                    <li>
                                        <input type="checkbox" id="25" name="25" />
                                        <label class="checkbox-label" for="25">25,00€ - 50,00€</label></li>
                                    <li>
                                        <input type="checkbox" id="50" name="50" />
                                        <label class="checkbox-label" for="50">50,00€ - 100,00€</label></li>
                                    <li>
                                    <li>
                                        <input type="checkbox" id="100" name="100" />
                                        <label class="checkbox-label" for="100">100,00€ - 1000,00€</label></li>
                                    <li>
                                    <li>
                                        <input type="checkbox" id="1000" name="1000" />
                                        <label class="checkbox-label" for="1000">1000,00€ - 10000,00€</label></li>
                                    <li>
                                </ul>
                                <span class="filtre-cate"><a data-toggle="collapse" href="#collapseEval" aria-expanded="false">
                                    Evaluation</a>
                                </span>
                                <ul class="list-options collapse" id="collapseEval">
                                    <li>
                                        <input type="checkbox" id="5" name="5" />
                                        <label class="checkbox-label" for="5">5 <span class="fa fa-star checked"></span>
                                        </label></li>
                                    <li>
                                        <input type="checkbox" id="4" name="4" />
                                        <label class="checkbox-label" for="4">4 <span class="fa fa-star checked"></span> ou plus
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="3" name="3" />
                                        <label class="checkbox-label" for="3">3 <span class="fa fa-star checked"></span> ou plus
                                        </label>
                                    </li>
                                </ul>

                            </aside>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="mx-auto col-md-5 carreau cadre">
                                        <a href="<?= URL ?>pages/page_produit_carrelage.php" title ="Carreaux de ciments">
                                            <img src='<?= URL ?>img/catalogue_carrelages/carreauCiment1.jpg' alt="image carrelage">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Carreaux de ciment imitations</p>
                                                <p class='prix'>19.99 &euro; TTC / m²</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_carrelage.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_carrelage.php" title ="Carreaux de ciments">
                                            <img src='<?= URL ?>img/catalogue_carrelages/carreauCiment2.jpg'>
                                        </a>
                                        <p><span>promo - 20%</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Carreaux de ciment imitations</p>
                                                <p class='prix'>19.99 &euro; TTC / m²</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_carrelage.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_carrelage.php" title ="Carreaux de ciments">
                                            <img src='<?= URL ?>img/catalogue_carrelages/carreauCiment3.jpg'>
                                        </a>
                                        <p><span>livraison offerte</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Carreaux de ciment imitations</p>
                                                <p class='prix'>19.99 &euro; TTC / m²</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_carrelage.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre offset-md-2 side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_carrelage.php" title ="Carreaux de ciments">
                                            <img src='<?= URL ?>img/catalogue_carrelages/carreauCiment4.jpg'>
                                        </a>
                                        <p><span>livraison offerte</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Carreaux de ciment imitations</p>
                                                <p class='prix'>19.99 &euro; TTC / m²</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_carrelage.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_carrelage.php" title ="Carreaux de ciments">
                                            <img src='<?= URL ?>img/catalogue_carrelages/carreauCiment5.jpg'>
                                        </a>
                                        <p><span>livraison offerte</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Carreaux de ciment imitations</p>
                                                <p class='prix'>19.99 &euro; TTC / m²</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_carrelage.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_carrelage.php" title ="Carreaux de ciments">
                                            <img src='<?= URL ?>img/catalogue_carrelages/carreauCiment6.jpg'>
                                        </a>
                                        <p><span>livraison offerte</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Carreaux de ciment imitations</p>
                                                <p class='prix'>19.99 &euro; TTC / m²</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_carrelage.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_plancher.php" title ="Parquet chêne">
                                            <img src='<?= URL ?>img/catalogue_plancher/img1.jpg' alt ="Parquet chêne">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Parquet chêne</p>
                                                <p class='prix'>39.99 € TTC / m²</p>
                                            </div>
                                        </div>
                                        <p><span>promo - 20%</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_plancher.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href<?= URL ?>pages/page_devis.php"#" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_plancher.php" title ="Parquet chêne blanchi">
                                            <img src='<?= URL ?>img/catalogue_plancher/img2.jpg'  alt ="Parquet chêne blanchi">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Parquet chêne blanchi</p>
                                                <p class='prix'>49.99 € TTC / m²</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_plancher.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_plancher.php" title ="Contrecollé chêne, vitrifié">
                                            <img src='<?= URL ?>img/catalogue_plancher/img3.jpg' alt ="Contrecollé chêne, vitrifié">
                                        </a>
                                        <p><span>livraison offerte</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Contrecollé chêne, vitrifié</p>
                                                <p class='prix'>19.99 € TTC / m²</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_plancher.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre offset-md-2 side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_plancher.php" title ="Stratifié Lazio">
                                            <img src='<?= URL ?>img/catalogue_plancher/img4.jpg' alt ="Stratifié Lazio">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Stratifié Lazio</p>
                                                <p class='prix'>19.99 € TTC / m²</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_plancher.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_plancher.php" title ="Plancher brut en pin">
                                            <img src='<?= URL ?>img/catalogue_plancher/img5.jpg' alt ="Plancher brut en pin">
                                        </a>
                                        <p><span>livraison offerte</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Plancher brut en Pin</p>
                                                <p class='prix'>59.99 € TTC / m²</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_plancher.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_plancher.php" title ="plancher massif sapin">
                                            <img src='<?= URL ?>img/catalogue_plancher/img6.jpg' alt ="plancher massif sapin">
                                        </a>
                                        <p><span>livraison offerte</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Plancher massif sapin</p>
                                                <p class='prix'>19.99 € TTC / m²</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_plancher.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_exterieur.php" title ="Piscine extérieure">
                                            <img src='<?= URL ?>img/catalogue_exterieur/img1.jpg' alt ="Parquet chêne">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Piscine extérieure hors sol</p>
                                                <p class='prix'>4999 € TTC</p>
                                            </div>
                                        </div>

                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_exterieur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_exterieur.php" title ="Cheminée mexicaine">
                                            <img src='<?= URL ?>img/catalogue_exterieur/img2.jpg'  alt ="Cheminée mexicaine">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Cheminée mexicaine</p>
                                                <p class='prix'>159 € TTC</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_exterieur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_exterieur.php" title ="Salon de jardin St tropez taupe">
                                            <img src='<?= URL ?>img/catalogue_exterieur/img3.jpg' alt ="Salon de jardin St tropez taupe">
                                        </a>
                                        <p><span>livraison offerte</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Salon de jardin St tropez taupe</p>
                                                <p class='prix'>716 € TTC l'ensemble</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_exterieur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="#" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="#" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre offset-md-2 side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_exterieur.php" title ="Aire de jeux bois 2 swing extra TRIGANO">
                                            <img src='<?= URL ?>img/catalogue_exterieur/img4.jpg' alt ="Aire de jeux bois 2 swing extra TRIGANO">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Aire de jeux bois 2 swing extra TRIGANO</p>
                                                <p class='prix'>879 € TTC / unité</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_exterieur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_exterieur.php" title ="Fontaine robinet en zinc">
                                            <img src='<?= URL ?>img/catalogue_exterieur/img5.jpg' alt ="Fontaine robinet en zinc">
                                        </a>
                                        <p><span>livraison offerte</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Fontaine robinet en zinc</p>
                                                <p class='prix'>1499.99 € TTC / unité</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_exterieur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_exterieur.php" title ="Abri de jardin Kerno axess">
                                            <img src='<?= URL ?>img/catalogue_exterieur/img6.jpg' alt ="Abri de jardin Kerno axess">
                                        </a>
                                        <p><span>290 € d'économie</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Abri de jardin Kerno axess</p>
                                                <p class='prix'>989 € / unité</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_exterieur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_mur.php" title ="Panneau roulé en laine de verre">
                                            <img src='<?= URL ?>img/catalogue_mur/img1.jpg' alt ="Panneau roulé en laine de verre">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Panneau roulé en laine de verre</p>
                                                <p class='prix'>28,60 € TTC</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_mur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_mur.php" title="Plaque de plâtre archi MO H1">
                                            <img src='<?= URL ?>img/catalogue_mur/img2.jpg'  alt ="Plaque de plâtre archi MO H1">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Plaque de plâtre archi MO H1</p>
                                                <p class='prix'>19,95 € TTC</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_mur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_mur.php" title ="Lambris PVC Décor mural bois naturel ">
                                            <img src='<?= URL ?>img/catalogue_mur/img3.jpg' alt ="Lambris PVC Décor mural bois naturel ">
                                        </a>

                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Lambris PVC Décor mural bois naturel </p>
                                                <p class='prix'>79,90 € TTC</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_mur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre offset-md-2 side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_mur.php" title ="Carrelage mur gris, Vestige">
                                            <img src='<?= URL ?>img/catalogue_mur/img4.jpg' alt ="Carrelage mur gris, Vestige">
                                        </a>
                                        <p><span>- 26 %</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Carrelage mur gris, Vestige</p>
                                                <p class='prix'>19,90 € TTC / m²</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_mur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_mur.php" title ="Brique creuse">
                                            <img src='<?= URL ?>img/catalogue_mur/img5.jpg' alt ="Brique creuse">
                                        </a>

                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Brique creuse</p>
                                                <p class='prix'>2,05 € TTC / unité</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_mur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_mur.php" title ="Brique de verre">
                                            <img src='<?= URL ?>img/catalogue_mur/img6.jpg' alt ="Brique de verre">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Brique de verre</p>
                                                <p class='prix'>1,69 € / unité</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_mur.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_accessoire.php" title ="Peinture blanc satin RIPOLIN">
                                            <img src='<?= URL ?>img/catalogue_accessoires/img1.jpg' alt ="Peinture blanc satin RIPOLIN">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Peinture blanc satin RIPOLIN</p>
                                                <p class='prix'>37,96 € TTC</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_accessoire.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_accessoire.php" title="Pinceau plat pour peindre">
                                            <img src='<?= URL ?>img/catalogue_accessoires/img2.jpg'  alt ="Pinceau plat pour peindre">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Pinceau plat pour peindre</p>
                                                <p class='prix'>1,50 € TTC</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_accessoire.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_accessoire.php" title ="Lambris PVC Décor mural bois naturel ">
                                            <img src='<?= URL ?>img/catalogue_accessoires/img3.jpg' alt ="Lambris PVC Décor mural bois naturel ">
                                        </a>

                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Combinaison de bricolage </p>
                                                <p class='prix'>2,80 € TTC</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_accessoire.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre offset-md-2 side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_accessoire.php" title ="Rouleaux adhésifs SCOTCH BLUE">
                                            <img src='<?= URL ?>img/catalogue_accessoires/img4.jpg' alt ="Rouleaux adhésifs SCOTCH BLUE">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Rouleaux adhésifs SCOTCH BLUE</p>
                                                <p class='prix'>14,90 € TTC / m²</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_accessoire.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_accessoire.php" title ="Masques antipoussières 3M">
                                            <img src='<?= URL ?>img/catalogue_accessoires/img5.jpg' alt ="Masques antipoussières 3M">
                                        </a>

                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Masques antipoussières 3M</p>
                                                <p class='prix'>2,05 € TTC / unité</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_accessoire.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_mur.php" title ="Plateforme de travail multifonction">
                                            <img src='<?= URL ?>img/catalogue_accessoires/img6.jpg' alt ="Plateforme de travail multifonction">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Plateforme de travail multifonction</p>
                                                <p class='prix'>60,90 €</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_accessoire.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_sanitaires.php" title ="Pack WC suspendu bâti universel">
                                            <img src='<?= URL ?>img/catalogue_sanitaire/img1.jpg' alt ="Pack WC suspendu bâti universel">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Pack WC suspendu bâti universel</p>
                                                <p class='prix'>299 € TTC</p>
                                            </div>
                                        </div>
                                        <p><span>60 € d'économie</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_sanitaire.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_sanitaires.php" title ="Cabine de douche rectangulaire">
                                            <img src='<?= URL ?>img/catalogue_sanitaire/img2.jpg'  alt ="Cabine de douche rectangulaire">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Cabine de douche rectangulaire</p>
                                                <p class='prix'>749 € TTC</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_sanitaires.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_sanitaires.php" title ="Mitigeur thermostatique douche">
                                            <img src='<?= URL ?>img/catalogue_sanitaire/img3.jpg' alt ="Mitigeur thermostatique douche">
                                        </a>

                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Mitigeur thermostatique douche</p>
                                                <p class='prix'>149,99 € TTC</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_sanitaires.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre offset-md-2 side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_exterieur.php" title ="Baignoire balnéo, Bora Bora">
                                            <img src='<?= URL ?>img/catalogue_sanitaire/img4.jpg' alt ="Baignoire balnéo, Bora Bora">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Baignoire balnéo, Bora Bora</p>
                                                <p class='prix'>1590 € TTC</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_sanitaires.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="mx-auto col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_sanitaires.php" title ="Plafonnier Icaria">
                                            <img src='<?= URL ?>img/catalogue_sanitaire/img5.jpg' alt ="Plafonnier Icaria">
                                        </a>
                                        <p><span>livraison offerte</span></p>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Plafonnier Icaria</p>
                                                <p class='prix'>119 € TTC</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_sanitaires.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 carreau cadre side-corner-tag">
                                        <a href="<?= URL ?>pages/page_produit_sanitaires.php" title ="Meuble vasque">
                                            <img src='<?= URL ?>img/catalogue_sanitaire/img6.jpg' alt ="Meuble vasque">
                                        </a>
                                        <div class="row carreau">
                                            <div class='col-md-12'>
                                                <p class='title_produit'>Meuble vasque rouge, Remix</p>
                                                <p class='prix'>409 €</p>
                                            </div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_sanitaires.php" title="detail produit">Voir le produit</a></span></div>
                                            <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                        </div>
                                        <div class="row carreau">
                                            <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                        </div>
                                    </div>
                                </div>
                                <nav aria-label="Page navigation example" class="nav-pagination">
                                    <ul class="pagination">
                                        <li class="page-item">
                                            <a class="page-link" href="#" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                        </li>
                                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item">
                                            <a class="page-link" href="#" aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                                <div class="scroll-top-wrapper ">
                                    <span class="scroll-top-inner">
                                        <i class="fa fa-2x fa-arrow-circle-up"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </main>
        </div>
        <?php require "../pages/footer.html"; ?>
        <!-- FIXED HEADER -->
        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function(){
                var offset = $(".menu").offset().top;
                $(document).scroll(function(){
                    var scrollTop = $(document).scrollTop();
                    if(scrollTop > offset){
                        $(".menu").addClass('fixed-top');
                    }
                    else {
                        $(".menu").removeClass('fixed-top');
                    }
                });
            });
        </script>
        <!-- SCROLL TO TOP -->
        <script>
            $(document).ready(function(){

                $(function(){

                    $(document).on( 'scroll', function(){

                        if ($(window).scrollTop() > 100) {
                            $('.scroll-top-wrapper').addClass('show');
                        } else {
                            $('.scroll-top-wrapper').removeClass('show');
                        }
                    });

                    $('.scroll-top-wrapper').on('click', scrollToTop);
                });

                function scrollToTop() {
                    verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
                    element = $('body');
                    offset = element.offset();
                    offsetTop = offset.top;
                    $('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
                }

            });
        </script>

    </body>
</html>