<?php require_once("init.inc.php") ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no">
        <title>ESHOP</title>
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,500,500i,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link href="../css/style.css" rel="stylesheet" type="text/css">
        <link href="../css/style_qui_sommes_nous.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <?php require "../pages/header.html"; ?>
            <!--MAIN-->            
            <!-- Main jumbotron for a primary marketing message or call to action -->
            <div class="row">
                <div class="jumbotron col-md-12">
                    <div class="container">
                        <h1 class="title-univers">Eshop, lier l'habitat et l'humain </h1>
                        <p>Au cours de ses 4 siècles d'existence, Eshop n'a eu de cesse de lier habitat, confort et humain.</p>
                    </div>
                </div>
            </div>
            <div class="container">
                <!-- Example row of columns -->
                <div class="row encart-univers mb-5">
                    <div class="col-md-8 offset-2 mb-3">
                        <h1 class="text-center"><span class="square square-padding">E</span>SHOP</h1>
                    </div>
                    <div class="col-md-4"><h3>Fondée en 1789</h3></div>
                    <div class="col-md-4"><h3>3.2Trd € de CA</h3></div>
                    <div class="col-md-4"><h3>2 magasins</h3></div>
                </div>
                <div class="row histo-univers mb-3">
                    <div class="col-md-1 date-univers offset-md-2">
                        1789
                    </div>
                    <div class="col-md-7 text-univers">
                        <p>Alors que le roi est emprisonné à la Bastille par un bel après-midi ensoleillé, celui-ci est visité par le fantôme d'Obi-Wan Kenobi. Ce dernier lui souffle l'idée de créer un eshop internet sur le bricolage. Comme internet n'existait pas à l'époque il fondit une société secrète qui transmettrait cette idée de génération en génération jusqu'à ce que l'univers soit prêt à sa réalisation.</p>
                    </div>
                    <hr class="line-bottom">
                </div>
                <div class="row histo-univers mb-3">
                    <div class="col-md-1 date-univers offset-md-2">
                        1997
                    </div>
                    <div class="col-md-7 text-univers">
                        <p>Cette année-là, la comète Halo-Biwan passe dans le ciel. Les membres de la société y voient là le signe qu'ils attendaient. Ils décident donc de créer le site et pour cela ils demandent aux élèves de l'IFOCOP de le réaliser (contre des codes promo, parce que faut pas déconner, avec les impôts qui viennent de tomber on a plus de fric...)</p>
                    </div>
                    <hr class="line-bottom">

                </div>
                <div class="row histo-univers mb-3">
                    <div class="col-md-1 date-univers offset-md-2">
                        1998
                    </div>
                    <div class="col-md-7 text-univers">
                        <p>8 mois plus tard et 50 retours clients après, le site sort enfin. Dès la mise en ligne c'est un énorme succès. Leur best-seller : une piscine en forme de leur maître Obi-Wan Kenobi. On se refait pas...</p>
                    </div>
                    <hr class="line-bottom">

                </div>
                <div class="row histo-univers mb-5">
                    <div class="col-md-1 date-univers offset-md-2">
                        2018
                    </div>
                    <div class="col-md-7 text-univers">
                        <p>20 ans ont passé et l'eshop Eshop (franchement niveau orginalité on a vu mieux...) accumule les milliards de vente. Même Donald Trump a sa carte de membre. Prochain étape : créer une tartine qui ne tomberait plus du côté beurré...</p>
                    </div>
                </div>   
                <!--<div class="row mb-5">
                    <div class="col-md-2 img-montage offset-md-2">
                        <img src="../img/qui_sommes_nous/montage-meuble.jpg">
                    </div>
                    <div class="col-md-6 legende-montage"><span>Un bref aperçu des possibilités infinies avec nos armoirs KLX-C-137</span></div>
                </div> -->            
            </div>

        </div>
        <?php require "../pages/footer.html"; ?>

        <!-- FIXED HEADER -->

        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function(){
                var offset = $(".menu").offset().top;
                $(document).scroll(function(){
                    var scrollTop = $(document).scrollTop();
                    if(scrollTop > offset){
                        $(".menu").addClass('fixed-top');
                    }
                    else {
                        $(".menu").removeClass('fixed-top');
                    }
                });
            });
        </script>
    </body>
</html>