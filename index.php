<?php require_once("pages/init.inc.php") ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no">
        <title>ESHOP</title>
        <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,500,500i,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link href="<?= URL ?>css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <?php require "pages/header.html"; ?>
            <!--MAIN-->
            <!--CAROUSEL-->
            <main class="container">
                <div class="row">
                    <section id="carousel_indicator" class="carousel slide col-lg-9" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel_indicator" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel_indicator" data-slide-to="1"></li>
                            <li data-target="#carousel_indicator" data-slide-to="2"></li>
                            <li data-target="#carousel_indicator" data-slide-to="3"></li>
                            <li data-target="#carousel_indicator" data-slide-to="4"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <a href="#" title="Carrelage mise en scène">
                                    <img class="d-block w-100" src="<?= URL ?>img/homepage/slide_1.jpg" alt="First slide">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Notre carrelage mis en scène dans la maison d'un nouveau client heureux !</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="carousel-item">
                                <a href="#" title="Découvrir produits">
                                    <img class="d-block w-100" src="<?= URL ?>img/homepage/slide_2.jpg" alt="Second slide">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>
                                            Venez découvrir tous nos produits directement en boutique !
                                        </h5>
                                    </div>
                                </a>
                            </div>
                            <div class="carousel-item">
                                <a href="#" title="Joies de la décoration">
                                    <img class="d-block w-100" src="<?= URL ?>img/homepage/slide_3.jpg" alt="Third slide">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Redécouvrez les joies de la décoration d'intérieur</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="carousel-item">
                                <a href="#" title="Découvrir les parquets">
                                    <img class="d-block w-100" src="<?= URL ?>img/homepage/slide_4.jpg" alt="Fourth slide">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Venez découvrir nos parquets</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="carousel-item">
                                <a href="#" title="Découvrir les produits">
                                    <img class="d-block w-100" src="<?= URL ?>img/homepage/slide_5.jpg" alt="Fifth slide">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Venez découvrir tous nos produits directement en boutique !
                                        </h5>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carousel_indicator" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel_indicator" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </section>
                    <!--ASIDE-->
                    <div class="col-lg-3 col-md-12">
                        <div class="row">
                            <aside class="col-lg-11 offset-lg-1 col-md-5 offset-md-0 main_categorie_carrelages"><span class="libelle_categorie"><a href="<?= URL ?>pages/page_catalogue_carrelage.php" title="voir carrelages">Voir tous les carrelages</a></span><img src="<?= URL ?>img/homepage/section_carrelage.jpg" alt="tous les carrelages"></aside>
                            <aside class="col-lg-11 offset-lg-1 col-md-5 offset-md-2 main_categorie"><span class="libelle_categorie"><a href="<?= URL ?>pages/page_catalogue_plancher.php" title="voir parquets">Voir tous les parquets</a></span><img src="<?= URL ?>img/homepage/section_parquet.jpg" alt="tous les parquets"></aside>
                        </div>
                    </div>
                </div>
                <!--REASSURANCE-->
                <div class="row reassurance">
                    <div class="col-lg-2 col-md-4 col-sm-6 offset-sm-0 col-6 offset-3">
                        <img src="<?= URL ?>img/homepage/icon_paiement.jpg">
                        <span class="rea_texte">paiement sécurisé</span>
                    </div>
                    <div class="col-lg-2 offset-lg-1 col-md-4 offset-sm-0 col-sm-6 col-6 offset-3">
                        <img src="<?= URL ?>img/homepage/icon_casse.jpg">
                        <span class="rea_texte">garantie anti-casse</span>
                    </div>
                    <div class="col-lg-2 offset-lg-1 col-md-4 offset-sm-0 col-sm-6 col-6 offset-3">
                        <img src="<?= URL ?>img/homepage/icon_satisfait.jpg">
                        <span class="rea_texte">satisfait ou remboursé</span>
                    </div>
                    <div class="col-lg-3 offset-lg-1 col-md-4 offset-sm-0 last_rea col-sm-6 col-6 offset-3">
                        <img src="<?= URL ?>img/homepage/icon_paiement_ss_frais.jpg">
                        <span class="rea_texte">facilité de paiement<br>3x sans frais dès XXX€</span>
                    </div>

                    <div class="col-lg-3 col-md-4 col-sm-6 offset-sm-0 col-6 offset-3">
                        <img src="<?= URL ?>img/homepage/icon_livraison.jpg">
                        <span class="rea_texte">livraison à domicile</span>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 offset-sm-0 col-6 offset-3">
                        <img src="<?= URL ?>img/homepage/icon_echantillon.jpg">
                        <span class="rea_texte">échantillon gratuit</span>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 offset-sm-0 col-6 offset-3">
                        <img src="<?= URL ?>img/homepage/icon_echange.jpg">
                        <span class="rea_texte">échange et retour facile</span>
                    </div>
                </div>

                <!--NOS PRODUITS-->
                <section class="row ligne_produits">
                    <div class="col-12">
                        <h3 class="title-section-produits">nos produits</h3>
                        <hr>
                        <div class="row">

                            <div class="col-md-3 carreau cadre">
                                <a href="<?= URL ?>pages/page_produit_carrelage.php" title ="Carreaux de ciments">
                                    <img src='img/catalogue_carrelages/carreauCiment1.jpg'>
                                </a>
                                <div class="row carreau">
                                    <div class='col-md-12'>
                                        <p class='title_produit'>Carreaux de ciment imitations</p>
                                        <p class='prix'>19.99 € TTC / m²</p>
                                    </div>
                                </div>
                                <div class="row carreau">
                                    <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_carrelage.php" title="detail produit">Voir le produit</a></span></div>
                                    <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                </div>
                                <div class="row carreau">
                                    <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                </div>
                            </div>
                            <div class="col-md-3 carreau cadre mx-auto side-corner-tag">
                                <a href="<?= URL ?>pages/page_produit_carrelage.php" title ="Carreaux de ciments">
                                    <img src='img/catalogue_carrelages/carreauCiment2.jpg'>
                                </a>
                                <p><span>promo - 20%</span></p>
                                <div class="row carreau">
                                    <div class='col-md-12'>
                                        <p class='title_produit'>Carreaux de ciment imitations</p>
                                        <p class='prix'>19.99 € TTC / m²</p>
                                    </div>
                                </div>
                                <div class="row carreau">
                                    <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_carrelage.php" title="detail produit">Voir le produit</a></span></div>
                                    <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                </div>
                                <div class="row carreau">
                                    <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                </div>
                            </div>
                            <div class="col-md-3 carreau cadre side-corner-tag">
                                <a href="<?= URL ?>pages/page_produit_carrelage.php" title ="Carreaux de ciments">
                                    <img src='img/catalogue_carrelages/carreauCiment3.jpg'>
                                </a>
                                <p><span>livraison offerte</span></p>
                                <div class="row carreau">
                                    <div class='col-md-12'>
                                        <p class='title_produit'>Carreaux de ciment imitations</p>
                                        <p class='prix'>19.99 € TTC / m²</p>
                                    </div>
                                </div>
                                <div class="row carreau">
                                    <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_carrelage.php" title="detail produit">Voir le produit</a></span></div>
                                    <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                                </div>
                                <div class="row carreau">
                                    <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class='ligne_produits'>
                    <div class="row">

                        <div class="col-md-3 carreau cadre">
                            <a href="<?= URL ?>pages/page_produit_carrelage.php" title ="Carreaux de ciments">
                                <img src='img/catalogue_carrelages/carreauCiment4.jpg'>
                            </a>
                            <div class="row carreau">
                                <div class='col-md-12'>
                                    <p class='title_produit'>Carreaux de ciment imitations</p>
                                    <p class='prix'>19.99 € TTC / m²</p>
                                </div>
                            </div>
                            <div class="row carreau">
                                <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_carrelage.php" title="detail produit">Voir le produit</a></span></div>
                                <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                            </div>
                            <div class="row carreau">
                                <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                            </div>
                        </div>
                        <div class="col-md-3 carreau cadre mx-auto">
                            <a href="<?= URL ?>pages/page_produit_carrelage.php" title ="Carreaux de ciments">
                                <img src='img/catalogue_carrelages/carreauCiment2.jpg'>
                            </a>
                            <div class="row carreau">
                                <div class='col-md-12'>
                                    <p class='title_produit'>Carreaux de ciment imitations</p>
                                    <p class='prix'>19.99 € TTC / m²</p>
                                </div>
                            </div>
                            <div class="row carreau">
                                <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_carrelage.php" title="detail produit">Voir le produit</a></span></div>
                                <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                            </div>
                            <div class="row carreau">
                                <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                            </div>
                        </div>
                        <div class="col-md-3 carreau cadre side-corner-tag">
                            <a href="<?= URL ?>pages/page_produit_carrelage.php" title ="Carreaux de ciments">
                                <img src='img/catalogue_carrelages/carreauCiment3.jpg'>
                            </a>
                            <p><span>nouveauté</span></p>
                            <div class="row carreau">
                                <div class='col-md-12'>
                                    <p class='title_produit'>Carreaux de ciment imitations</p>
                                    <p class='prix'>19.99 € TTC / m²</p>
                                </div>
                            </div>
                            <div class="row carreau">
                                <div class='col-md-6  voir_produit'><span><a href="<?= URL ?>pages/page_produit_carrelage.php" title="detail produit">Voir le produit</a></span></div>
                                <div class='col-md-6 ajout_panier'><span><a href="<?= URL ?>pages/page_mon_panier.php" title="ajouter produit au panier">Ajout au panier</a></span></div>
                            </div>
                            <div class="row carreau">
                                <div class='col-md-12 devis_perso'><span class="devis_icon"><a href="<?= URL ?>pages/page_devis.php" title="demander devis">Demande de devis personnalisé</a></span></div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--ESHOP INFOS-->
                <section class="row eshop_infos">
                    <div class="col-12">
                        <h3 class="title-section-eshop">eshop</h3>
                        <div class="row eshop_content">
                            <div class="col-xl-3 col-lg-4 eshop_col">
                                <div class="eshop_label">
                                    <img src="<?= URL ?>img/homepage/eshop_icon_adresse.jpg" width="35px" alt="icone adresse">
                                    <span class="marge_legende"><span class="eshop_legende font-weight-bold">Adresse :<br></span>Avenue Leclerc<br>75015 Paris</span></div>
                                <div class="eshop_label">
                                    <img src="<?= URL ?>img/homepage/eshop_icon_phone.jpg" width="35px" alt="icone adresse">
                                    <span class="marge_legende"><span class="eshop_legende font-weight-bold">Téléphone :<br></span>Tél : 01 28 02 29 45<br>Fax : 01 60 05 12 33</span></div>
                                <div class="eshop_label email">
                                    <img src="<?= URL ?>img/homepage/eshop_icon_mail.jpg" width="35px" alt="icone adresse">
                                    <span class="marge_legende"><span class="eshop_legende font-weight-bold">Email :<br></span>contact@eshop.fr</span></div>
                            </div>
                            <div class="col-xl-3 offset-xl-1 col-lg-4 offset-lg-0 separateur">
                                <div class="eshop_label">
                                    <img src="<?= URL ?>img/homepage/eshop_icon_horaire.jpg" width="35px" alt="icone adresse">
                                    <span class="marge_legende"><span class="eshop_legende font-weight-bold">Horaires d'ouverture :<br></span>du lundi au vendredi de 8h à 21h et de 13h30 à 18h00<br><br>le samedi de 9h à 17h sans interruption<br><br>Service de livraison et enlèvement des marchandises jusqu'à 18h et le samedi jusqu'à 17h</span></div>
                            </div>
                            <div class="col-xl-4 offset-xl-1 col-lg-4 offset-lg-0 separateur">
                                <div class="eshop_label">
                                    <img src="<?= URL ?>img/homepage/eshop_icon_who.jpg" width="35px" alt="icone adresse">
                                    <span class="marge_legende"><span class="eshop_legende font-weight-bold">Qui sommes-nous ?<br></span>Spécialiste du carrelage et du parquet, ESHOP vous accompagne dans vos projets de construction ou de rénovation.<br><br>Entreprise familiale depuis 35 ans, nous possédons l'expérience et le savoir-faire alliant méthode traditionnelles et techniques actuelles </span></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 eshop_decouverte">Venez découvrir tous nos produits directement en boutique !</div>
                        </div>

                    </div>
                </section>
            </main>
        </div>
        <?php require "pages/footer.html"; ?>
        <!-- FIXED HEADER -->

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>

            $(document).ready(function(){
                var offset = $(".menu").offset().top;
                $(document).scroll(function(){
                    var scrollTop = $(document).scrollTop();
                    if(scrollTop > offset){
                        $(".menu").addClass('fixed-top');
                    }
                    else {
                        $(".menu").removeClass('fixed-top');
                    }
                });
            });

        </script>

    </body>
</html>